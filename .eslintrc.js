module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:import/recommended"
    ],
    "parser": "babel-eslint",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true,
            "experimentalObjectRestSpread": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "semi": ["error", "always"],
        // "indent": ["error", "tab"],
        "quotes": ["error", "single"],
        "max-len": ["error", { "code": 120 }],
        "max-params": ["error", 3],
        "no-magic-numbers": ["error", {"ignore": [0, 1]}],
        "object-curly-spacing": ["error", "always"],
        "array-bracket-spacing": ["error", "always"],
        "no-mixed-spaces-and-tabs": ["error", "smart-tabs"],
        "import/newline-after-import": ["error", { "count": 2 }],
        "import/no-unresolved": ["error", {"caseSensitive": true, "ignore": ["assets"]}],
        "react/boolean-prop-naming": 0,
        "no-console": 2,
    }
};