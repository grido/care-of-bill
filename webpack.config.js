const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const Autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const CleanWebpackPlugin = require('clean-webpack-plugin');


const buildPath = path.resolve(__dirname, 'public/build');
const publicPath = path.basename(require.main.filename).includes('dev-server') ?
    'http://localhost:3000/build/' :
    '/build/';

const polyfills = [
  'ie-array-find-polyfill',
];

function entryFile(fileName) {
  polyfills.forEach(function(pollyfill) {
    require(pollyfill);
  });

  return polyfills.concat([ fileName ]);
}

module.exports = function(env, options) {
  const isDevMode = options.mode === 'development';

  const stats = {
    colors: true,
    hash: false,
    timings: true,
    assets: false,
    modules: false,
    children: false,
  };

  return {
    stats: stats,
    devtool: isDevMode ? 'source-map' : false,
    entry: {
      // example: entryFile('./assets/example.js'),
      // example: ['./assets/scss/example.scss', ./assets/scss/example.css],
      app: entryFile('./assets/index.js'),
    },
    output: {
      path: buildPath,
      publicPath: publicPath,
      filename: isDevMode ? '[name].bundle.js' : '[name].[hash].bundle.js',
      // filename: 'example.bundle.js',
    },
    resolve: {
      modules: [
        path.resolve(__dirname, ''),
        path.resolve(__dirname, 'node_modules'),
      ],
    },
    module: {
      rules: [ {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [ 'babel-loader' ],
      },
      {
        test: /\.(c|sc|sa)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: isDevMode,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                Autoprefixer,
              ],
              sourceMap: isDevMode,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDevMode,
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          },
        },
      },
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        use: [ {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'images/',
          },
        } ],
      },
      ],
    },
    devServer: {
      stats: stats,
      contentBase: path.resolve(__dirname, 'public'),
      headers: { 'Access-Control-Allow-Origin': '*' },
    },
    optimization: {
      minimize: !isDevMode,
      minimizer: [
        new TerserPlugin(),
        new OptimizeCSSAssetsPlugin({}),
      ],
    },
    plugins: [
      new FixStyleOnlyEntriesPlugin(),
      new CleanWebpackPlugin([ buildPath ], {
        verbose: true,
      }),
      new MiniCssExtractPlugin({
        filename: isDevMode ? '[name].bundle.css' : '[name].[hash].bundle.css',
      }),
      new HtmlWebpackPlugin({
        alwaysWriteToDisk: true,
        template: path.resolve(__dirname, 'assets/index.html'),
        filename: path.resolve(__dirname, 'public/index.html'),
      }),
      new HtmlWebpackHarddiskPlugin(),
    ],
  };
};
