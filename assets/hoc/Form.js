import React, { Component } from 'react';


const Form = (WrappedComponent) => {
    class WithForm extends Component {
        state = {
            form: {}
        };

        onChange = e => {
            const target = e.target;

            const form = {
                [target.name]: target.value
            };

            this.setState(state => ({ form: { ...state.form, ...form } }));
        };

        render()
        {
            return <WrappedComponent
                {...this.props}
                {...this.state}
                onChange={this.onChange}
            />;
        }
    }

    return WithForm;
};

export default Form;