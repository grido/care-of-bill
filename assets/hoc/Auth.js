import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAuth, setTokens, getTokens, clearTokens, isTokenExpired, decodeToken, getTokenTimeout } from 'assets/util';
import { setUser } from 'assets/actions/userActions';


const Auth = (WrappedComponent) => {
    const propTypes = {
        isAuthenticated: PropTypes.bool,
        setUser: PropTypes.func
    };

    class WithAuth extends Component {
        state = {
            attempted: false
        };

        timeout = null;

        componentDidMount()
        {
            this.auth();
        }

        componentWillUnmount()
        {
            clearTimeout(this.timeout);
        }

        attempt = () => this.setState(() => ({ attempted: true }));

        auth = () => {
            const tokens = getTokens();

            if (!!tokens.accessToken && !isTokenExpired(tokens.accessToken)) {
                this.handleAuth(tokens);
            } else if (!!tokens.refreshToken && !isTokenExpired(tokens.refreshToken)) {
                this.refreshTokens(tokens.refreshToken);
            } else {
                this.attempt();
            }
        };

        handleAuth = (tokens) => {
            const { accessToken, refreshToken } = tokens;
            const decodedAccessToken = decodeToken(accessToken);

            if(!this.props.isAuthenticated) {
                this.props.setUser(decodedAccessToken.sub)
                    .then(() => {
                        this.initRefresh(refreshToken, decodedAccessToken);

                        this.attempt();
                    })
                    .catch(() => {
                        clearTokens();

                        this.attempt();
                    });
            } else {
                this.initRefresh(accessToken, decodedAccessToken);

                this.attempt();
            }
        };

        refreshTokens = (refreshToken) => {
            getAuth('refresh', refreshToken)
                .then((tokens) => {
                    setTokens(tokens);

                    this.handleAuth(tokens);
                })
                .catch(() => {
                    this.attempt();
                });
        };

        initRefresh = (refreshToken, decodedAccessToken) => {
            this.timeout = setTimeout(() => {
                this.refreshTokens(refreshToken);
            }, getTokenTimeout(decodedAccessToken));
        };

        render()
        {
            const { ...rest } = this.props;

            delete rest.setUser;

            if (!this.state.attempted) {
                return null;
            }

            return <WrappedComponent {...rest} />;
        }
    }

    WithAuth.propTypes = propTypes;

    const mapStateToProps = (state) => ({
        isAuthenticated: !!state.user.id,
    });

    const mapDispatchToProps = { setUser };

    return connect(mapStateToProps, mapDispatchToProps)(WithAuth);
};

export default Auth;