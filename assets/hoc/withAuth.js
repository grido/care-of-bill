import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addUser, removeUser } from 'assets/actions/userActions';
import {
    getAuth,
    postAuth,
    getApi,
    setTokens,
    getTokens,
    clearTokens,
    isTokenExpired,
    decodeToken,
    getTokenTimeout,
} from 'assets/util';


const withAuth = (WrappedComponent, privateComponent = true) => {
    const propTypes = {
        history: PropTypes.object.isRequired,
        // dispatch: PropTypes.func.isRequired,
        user: PropTypes.object,
        addUser: PropTypes.func,
        removeUser: PropTypes.func,
    };

    class WithAuth extends Component {
        state = {
            confirm: false,
            loggedIn: false,
        };

        timeout = null;

        componentDidMount()
        {
            this.auth();
        }

        componentDidUpdate()
        {
            const { props, state } = this;

            if (state.confirm && !state.loggedIn && privateComponent) {
                props.history.replace('/login');
            }
        }

        componentWillUnmount()
        {
            clearTimeout(this.timeout);
        }

        auth = () => {
            const tokens = getTokens();

            if (!!tokens.accessToken && !isTokenExpired(tokens.accessToken)) {
                this.handleAuth(tokens);
            } else if (!!tokens.refreshToken && !isTokenExpired(tokens.refreshToken)) {
                this.refresh(tokens.refreshToken);
            } else {
                this.confirm();
            }
        };

        handleAuth = (tokens) => {
            const decodedAccessToken = decodeToken(tokens.accessToken);

            this.initRefreshAction(tokens.refreshToken, decodedAccessToken);

            return this.setAuthUser(decodedAccessToken);
        };

        confirm = (loggedIn = false) => {
            if (!this.state.confirm || this.state.loggedIn !== loggedIn) {
                this.setState({ confirm: true, loggedIn });
            }
        };

        initRefreshAction = (refreshToken, decodedAccessToken) => {
            this.timeout = setTimeout(() => {
                this.refresh(refreshToken);
                // }, 5000);
            }, getTokenTimeout(decodedAccessToken));
        };

        setAuthUser = (decodedAccessToken) => {
            if (!this.props.user.id) {
                return getApi('users', decodedAccessToken.sub)
                    .then((response) => {
                        const { data } = response;
                        return this.props.addUser(data);
                    })
                    .then(() => {
                        this.confirm(true);
                    });
            }

            this.confirm(true);
            return Promise.resolve();
        };

        refresh = (refreshToken) => {
            return getAuth('refresh', refreshToken)
                .then((response) => {
                    setTokens(response);
                    return this.handleAuth(response);
                })
                .catch(() => {
                    this.confirm();
                });
        };

        login = (email, password) => {
            return postAuth('authenticate', { email, password })
                .then((response) => {
                    setTokens(response);
                    return this.handleAuth(response);
                });
        };

        logout = () => {
            clearTokens();
            this.setState(
                () => ({ loggedIn: false }),
                () => this.props.removeUser()
            );
        };

        render()
        {
            const { props, state } = this;

            if (!state.confirm) {
                return null;
            }

            return <WrappedComponent
                {...props}
                {...state}
                login={this.login}
                logout={this.logout}
            />;
        }
    }

    WithAuth.propTypes = propTypes;

    const mapStateToProps = (state) => ({ user: state.user });

    const mapDispatchToProps = { addUser, removeUser };

    return connect(mapStateToProps, mapDispatchToProps)(WithAuth);
};

export default withAuth;
