import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import Auth from 'assets/hoc/Auth';


const propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired,
};

const PublicRoute = ({ isAuthenticated, component: Component, ...rest }) => (
    <Route {...rest} component={(props) => (
        !isAuthenticated ? <Component {...props} /> : <Redirect to="/"/>
    )}/>
);

PublicRoute.propTypes = propTypes;

export default Auth(PublicRoute);
