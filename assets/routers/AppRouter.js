import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import { DashboardPage, LoginPage } from 'assets/pages';
import PublicRoute from 'assets/routers/PublicRoute';
import PrivateRoute from 'assets/routers/PrivateRoute';


const AppRouter = () => {
    return (
        <BrowserRouter>
            <Switch>
                <PrivateRoute path="/" component={DashboardPage} exact={true}/>
                <PublicRoute path="/login" component={LoginPage}/>
            </Switch>
        </BrowserRouter>
    );
};

export default AppRouter;


