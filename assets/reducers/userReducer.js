const userReducerDefaultState = {
    id: 0,
    email: '',
    user_description: {}
};

export default (state = userReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return {
                ...action.user,
            };
        case 'REMOVE_USER':
            return userReducerDefaultState;
        default:
            return state;
    }
};
