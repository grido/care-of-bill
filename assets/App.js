import React from 'react';
import ThemeProvider from 'assets/providers/ThemeProvider';
import AppRouter from 'assets/routers/AppRouter';


const App = () => (
    <ThemeProvider>
        <AppRouter/>
    </ThemeProvider>
);

export default App;
