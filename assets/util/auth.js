import jwtDecode from 'jwt-decode';


const setTokens = tokens => {
    localStorage.setItem('accessToken', tokens.accessToken);
    localStorage.setItem('refreshToken', tokens.refreshToken);
};

const getTokens = () => ({
    accessToken: localStorage.getItem('accessToken'),
    refreshToken: localStorage.getItem('refreshToken')
});

const clearTokens = () => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
};

const isTokenExpired = token => {
    try {
        const decodedToken = jwtDecode(token);
        if (decodedToken.exp < Date.now() / 1000) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        return { hint: error };
    }
};

const decodeToken = token => {
    try {
        return jwtDecode(token);
    } catch (error) {
        return { hint: error };
    }
};

const getTokenTimeout = decodedToken => decodedToken.exp * 1000 - Date.now();

export { setTokens, getTokens, clearTokens, isTokenExpired, decodeToken, getTokenTimeout };