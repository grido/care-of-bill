const fetchJson = (url, options = {}) => {
    return fetch(url, Object.assign({
        credentials: 'same-origin',
    }, options))
        .then(checkStatus)
        .then(response => response.text())
        .then(data => data ? JSON.parse(data) : []);
};

const checkStatus = (response) => {

    if (response.status >= 200 && response.status < 400) {
        return response;
    }

    return response.text().then(error => {
        if (error) {
            let response;

            try {
                response = JSON.parse(error);
            } catch (e) {
                throw [ 'error response' ];
            }

            throw response;
        } else {
            throw [ '' ];
        }
    });
};

const authorization = () => {
    if (localStorage.getItem('accessToken')) {
        return { authorization: localStorage.getItem('accessToken') };
    }

    return {};
};

/**
 * @return {Promise<Response>}
 */
const getApi = (path, id = null) => {
    return fetchJson(`/api/${path}${id !== null ? `/${id}` : ''}`, {
        headers: {
            'Content-Type': 'application/json',
            ...authorization()
        }
    });
};

const postApi = (path, data) => {
    return fetchJson(`/api/${path}`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            ...authorization()
        }
    });
};

const patchApi = (path, id, data) => {
    return fetchJson(`/api/${path}/${id}`, {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            ...authorization()
        }
    });
};

const deleteApi = (path, id) => {
    return fetchJson(`/api/${path}/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            ...authorization()
        }
    });
};

const getAuth = (path, token) => {
    return fetchJson(`/auth/${path}`, {
        headers: {
            'Content-Type': 'application/json',
            'authorization': token
        }
    });
};

const postAuth = (path, data) => {
    return fetchJson(`/auth/${path}`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    });
};

export { getApi, postApi, patchApi, deleteApi, getAuth, postAuth };