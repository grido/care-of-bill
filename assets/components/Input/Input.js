import 'assets/components/Input/input.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    type: PropTypes.string,
    size: PropTypes.string,
    value: PropTypes.string,
    errors: PropTypes.array,
    tip: PropTypes.string,
    onChange: PropTypes.func
};

const defaultProps = {
    label: '',
    name: '',
    type: 'text',
    size: '',
    value: '',
    errors: [],
    tip: '',
    onChange: () => null
};

const Input = ({ label, name, type, size, value, errors, tip, onChange }) => {
    return (
        <div className={`form-input${value ? ' form-input--fill' : ''}`}>
            <input
                name={name}
                type={type}
                className={`form-input__field${size ? ' form-input--sm' : ''}`}
                value={value}
                onChange={onChange}
            />
            {!!label && <span className="form-input__label">{label}</span>}
            {!!errors.length && <span className="form-input__alert">
                        <i className="mdi mdi-alert-circle form-input__icon"></i>{errors[0]}
            </span>}
            {!!tip && <span className="form-input__tip">
                <i className="mdi mdi-tooltip-plus form-input__icon"></i>{tip}
            </span>}
        </div>
    );
};

Input.propTypes = propTypes;

Input.defaultProps = defaultProps;

export default Input;
