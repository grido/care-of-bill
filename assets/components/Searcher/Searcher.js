import 'assets/components/Searcher/searcher.scss';
import React from 'react';


const Searcher = () => {
    return (
        <div className="search-input">
            <input type="text" placeholder="Search" className="search-input__field"/>
            <i className="mdi mdi-magnify search-input__icon"></i>
            <i className="mdi mdi-window-close search-input__icon"></i>
        </div>
    );
};

export default Searcher;
