import 'assets/components/Tip/tip.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
    text: PropTypes.string.isRequired
};

const Tip = ({ children, text }) => {
    return (
        <div className="tip">
            {children}
            <span className="tip__text">
                {text}
            </span>
        </div>
    );
};

Tip.propTypes = propTypes;

export default Tip;
