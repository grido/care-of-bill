import 'assets/components/Nav/nav.scss';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { NavContext } from 'assets/providers/NavProvider';
import { ThemeContext } from 'assets/providers/ThemeProvider';
import { logout } from 'assets/actions/userActions';


const propTypes = {
  logout: PropTypes.func
};

const Nav = (props) => {
    const [ navOpen ] = useContext(NavContext);
    const [ ,, toggleTheme ] = useContext(ThemeContext);

    return (
        <nav className={`nav ${navOpen ? 'nav--active' : ''}`}>
            <ul className="nav-menu">
                <li className="nav-menu__item">
                    <NavLink
                        to="/"
                        className="nav-menu__link"
                        activeClassName="nav-menu__link--active"
                        exact={true}>
                        <i className="mdi mdi-home-import-outline nav-menu__icon"></i>
                        <span className="nav-menu__text">dashboard</span>
                    </NavLink>
                </li>
                <li className="nav-menu__item">
                    <a href="#" className="nav-menu__link">
                        <i className="mdi mdi-history nav-menu__icon"></i>
                        <span className="nav-menu__text">history</span>
                    </a>
                </li>
                <li className="nav-menu__item">
                    <a href="#" className="nav-menu__link">
                        <i className="mdi mdi-format-list-bulleted nav-menu__icon"></i>
                        <span className="nav-menu__text">products</span>
                    </a>
                </li>
                <li className="nav-menu__item">
                    <a className="nav-menu__link" onClick={props.logout}>
                        <i className="mdi mdi-logout nav-menu__icon"></i>
                        <span className="nav-menu__text">logout</span>
                    </a>
                </li>
                <li className={`nav-menu__item`}>
                    <a className="nav-menu__link" onClick={() => toggleTheme()}>
                        <i className="mdi mdi-logout nav-menu__icon"></i>
                        <span className="nav-menu__text">switcher</span>
                    </a>
                </li>
            </ul>
        </nav>
    );
};

Nav.propTypes = propTypes;

const mapDispatchToProps = { logout };

export default connect(null, mapDispatchToProps)(Nav);
