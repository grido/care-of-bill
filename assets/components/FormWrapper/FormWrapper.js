import 'assets/components/FormWrapper/formWrapper.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

const FormWrapper = ({ children }) => {
    return (
        <div className="form-wrapper">
            {children}
        </div>
    );
};

FormWrapper.propTypes = propTypes;

export default FormWrapper;
