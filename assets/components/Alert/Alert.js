import 'assets/components/Alert/alert.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    hint: PropTypes.shape({
        text: PropTypes.string
    }),
    type: PropTypes.string
};

class Alert extends Component {
    state = {
        open: null
    };

    timeout = null;

    componentDidMount()
    {
        if(this.props.hint.text) {
            this.toggle(true);
        }
    }

    componentDidUpdate(prevProps)
    {
        const { hint } = this.props;

        if(hint.text && hint !== prevProps.hint) {
            this.toggle(true);
        }

        if(!hint.text && prevProps.hint.text) {
            this.toggle();
        }
    }

    componentWillUnmount()
    {
        clearTimeout(this.timeout);
    }

    toggle = (open = false) => {
        const TIMEOUT = 5000;

        this.setState(() => ({ open }), () => {
            clearTimeout(this.timeout);

            if(open) {
                this.timeout = setTimeout(this.toggle, TIMEOUT);
            }
        });
    };

    render()
    {
        const { hint, type } = this.props;

        if (!this.state.open) {
            return null;
        }

        return (
            <div className={`alert${type ? ` alert--${type}` : ''}`}>
                <span>{hint.text}</span>
                <i className="mdi mdi-close alert__close" onClick={this.toggle}></i>
            </div>
        );
    }
}

Alert.propTypes = propTypes;

export default Alert;
