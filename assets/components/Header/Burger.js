import 'assets/components/Header/burger.scss';
import React, { useContext } from 'react';
import { NavContext } from 'assets/providers/NavProvider';


const Burger = () => {
    const [ navOpen, setNavOpen ] = useContext(NavContext);

    return (
        <div className={`burger ${navOpen ? 'burger--active' : ''}`} onClick={() => setNavOpen(!navOpen)}>
            <i className="mdi mdi-menu burger__icon"></i>
            <i className="mdi mdi-close burger__icon"></i>
        </div>
    );
};

export default Burger;
