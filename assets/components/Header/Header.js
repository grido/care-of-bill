import 'assets/components/Header/header.scss';
import React from 'react';
import { Searcher, Avatar } from 'assets/components';
import Burger from 'assets/components/Header/Burger';


const Header = () => (
    <header className="header">
        <Burger />

        <Searcher />

        <Avatar />
    </header>
);

export default Header;
