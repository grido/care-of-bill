import 'assets/components/Avatar/avatar.scss';
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Plate from 'assets/components/Avatar/Plate';


const propTypes = {
    user: PropTypes.shape({
        user_description: PropTypes.object
    })
};

const Avatar = ({ user }) => {
    const {
        user_description = {
            experience: 0,
            avatar: ''
        }
    } = user;

    return (

        <div className="avatar">
            <Plate text={`${user_description.experience}XP`}>
                {user_description.avatar
                    ? <img src={`https://www.gravatar.com/avatar/${user_description.avatar}?s=150`} alt="Avatar"
                           className="avatar__image"/>
                    : <i className="mdi mdi-account avatar__dummy"></i>
                }
            </Plate>
        </div>
    );
};

Avatar.propTypes = propTypes;

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps)(Avatar);