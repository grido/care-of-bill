import 'assets/components/Avatar/plate.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    text: PropTypes.string
};

const defaultProps = {
    text: ''
};

const Plate = ({ children, text }) => {
    return (
        <div className="plate">
            {children}
            {!!text && <span className="plate__text">{text}</span>}
        </div>
    );
};

Plate.propTypes = propTypes;

Plate.defaultProps = defaultProps;

export default Plate;
