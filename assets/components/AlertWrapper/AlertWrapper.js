import 'assets/components/AlertWrapper/alertWrapper.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

const AlertWrapper = ({ children }) => {
    return (
        <div className="alert-wrapper">
            {children}
        </div>
    );
};

AlertWrapper.propTypes = propTypes;

export default AlertWrapper;
