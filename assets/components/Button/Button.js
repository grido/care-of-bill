import 'assets/components/Button/button.scss';
import React from 'react';
import PropTypes from 'prop-types';


const propTypes = {
    children: PropTypes.string,
    size: PropTypes.string,
    onClick: PropTypes.func,
};

const defaultProps = {
    label: '',
    size: '',
    onClick: () => null
};

const Button = ({ children, size, onClick }) => {
    return (
        <button
            className={`btn${size ? ` btn--${size}` : ''}`}
            onClick={onClick}
        >{children}</button>
    );
};

Button.propTypes = propTypes;

Button.defaultPtops = defaultProps;

export default Button;