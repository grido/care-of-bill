import React from 'react';
import MainPrivateLayout from 'assets/layouts/MainPrivateLayout';


const DashboardPage = () => {
	return (
		<MainPrivateLayout>
			<p>Dashboard!</p>
		</MainPrivateLayout>
	);
};

export default DashboardPage;
