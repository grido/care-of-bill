import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MainPublicLayout from 'assets/layouts/MainPublicLayout';
import { FormWrapper, Input, Button, AlertWrapper, Alert } from 'assets/components';
import Form from 'assets/hoc/Form';
import { login } from 'assets/actions/userActions';


const propTypes = {
    history: PropTypes.object,
    isAuthenticated: PropTypes.bool,
    form: PropTypes.object,
    login: PropTypes.func,
    onChange: PropTypes.func,
};

class LoginPage extends Component {
    state = { hint: {} };

    handleForm = e => {
        e.preventDefault();

        const { form, login } = this.props;

        login(form.email, form.password)
            .catch(errors => {
                const hint = {
                    text: errors.hint,
                };

                this.setState(() => ({ hint }));
            });
    };

    render()
    {
        const { form, onChange } = this.props;
        const { hint } = this.state;

        return (
            <MainPublicLayout>
                <AlertWrapper>
                    <Alert hint={hint} type="error" />
                </AlertWrapper>
                <FormWrapper>
                    <form>
                        <h1>User Login</h1>
                        <div className="row self-center">
                            <i style={{ fontSize: '6.8rem' }} className="mdi mdi-login"></i>
                        </div>
                        <div className="row">
                            <Input
                                label="Email"
                                name="email"
                                value={form.email}
                                onChange={onChange}
                            />
                        </div>
                        <div className="row">
                            <Input
                                label="Password"
                                name="password"
                                type="password"
                                value={form.password}
                                onChange={onChange}
                            />
                        </div>
                        <div className="row self-end">
                            <Button
                                onClick={this.handleForm}
                            >Sign in</Button>
                        </div>
                    </form>
                </FormWrapper>
            </MainPublicLayout>
        );
    }
}

LoginPage.propTypes = propTypes;

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.user.id,
});

const mapDispatchToProps = { login };

export default connect(mapStateToProps, mapDispatchToProps)(Form(LoginPage));
