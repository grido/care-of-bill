import 'assets/style/vendor.scss';
import 'assets/style/base.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import Store from 'assets/store/store';
import { Provider } from 'react-redux';
import App from 'assets/App';


const store = Store();

// store.subscribe(() => console.log(store.getState()));

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(
    app,
    document.getElementById('app')
);