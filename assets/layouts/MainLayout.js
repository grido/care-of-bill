import 'assets/layouts/MainLayout.scss';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'assets/providers/ThemeProvider';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const MainLayout = ({ children }) => {
    const [ theme, transition ] = useContext(ThemeContext);

    return (
        <div className={`container ${transition ? 'container--transition' : ''}`} data-theme={theme}>
            {children}
        </div>
    );
};

MainLayout.propTypes = propTypes;

export default MainLayout;
