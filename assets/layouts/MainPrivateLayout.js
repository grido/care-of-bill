import React from 'react';
import PropTypes from 'prop-types';
import MainLayout from 'assets/layouts/MainLayout';
import NavProvider from 'assets/providers/NavProvider';
import { Header, Nav, Wrapper } from 'assets/components';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const MainPrivateLayout = ({ children }) => (
    <MainLayout>

        <NavProvider>

            <Header/>

            <Nav/>

        </NavProvider>

        <Wrapper>
            {children}
        </Wrapper>

    </MainLayout>
);

MainPrivateLayout.propTypes = propTypes;

export default MainPrivateLayout;
