import React from 'react';
import PropTypes from 'prop-types';
import MainLayout from 'assets/layouts/MainLayout';
import { Wrapper } from 'assets/components';


const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const MainPublicLayout = ({ children }) => (
    <MainLayout>
        <Wrapper>
            {children}
        </Wrapper>
    </MainLayout>
);

MainPublicLayout.propTypes = propTypes;

export default MainPublicLayout;
