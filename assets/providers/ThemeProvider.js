import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';
import variables from 'assets/style/_variables.scss';


const ThemeContext = createContext({});

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

class ThemeProvider extends Component {
    constructor(props)
    {
        super(props);

        const defaultTheme = localStorage.getItem('app-theme');

        this.state = {
            theme: defaultTheme ? defaultTheme : 'light',
            transition: false
        };
    }

    timeout = null;

    componentDidUpdate(prevProps, prevState)
    {
        if (prevState.theme !== this.state.theme) {
            this.changeTransition();
        }
    }

    componentWillUnmount()
    {
        clearTimeout(this.timeout);
    }

    changeTransition = () => {
        const TIMEOUT = variables['container-transition'];

        this.setState(
            () => ({ transition: true }),
            () => {
                clearTimeout(this.timeout);

                this.timeout = setTimeout(this.clearTransition, TIMEOUT);
            }
        );
    };

    clearTransition = () => this.setState(() => ({ transition: false }));

    toggleTheme = () => {
        this.setState(
            (prevState) => ({ theme: prevState.theme === 'light' ? 'dark' : 'light' }),
            () => localStorage.setItem('app-theme', this.state.theme)
        );
    };

    render()
    {
        const { theme, transition } = this.state;

        return (
            <ThemeContext.Provider value={[ theme, transition, this.toggleTheme ]}>
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}

ThemeProvider.propTypes = propTypes;

export { ThemeContext };

export default ThemeProvider;
