import React, { useState, useEffect, createContext } from 'react';
import PropTypes from 'prop-types';


const NavContext = createContext(null);

const propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const NavProvider = ({ children }) => {
    const navOpenDefaultState = Boolean(localStorage.getItem('nav-open'));

    const [ navOpen, setNavOpen ] = useState(navOpenDefaultState);

    useEffect(() => {
        localStorage.setItem('nav-open', navOpen ? '1' : '');
    });

    return (
        <NavContext.Provider value={[ navOpen, setNavOpen ]}>
            { children }
        </NavContext.Provider>
    );
};

NavProvider.propTypes = propTypes;

export { NavContext };

export default NavProvider;
