import { getApi, postAuth, clearTokens, setTokens, decodeToken } from 'assets/util';


const addUser = (user) => (
    {
        type: 'ADD_USER',
        user
    }
);

const removeUser = () => (
    {
        type: 'REMOVE_USER'
    }
);

const setUser = (id) => async (dispatch) => {
    const { data: user } = await getApi('users', id);

    dispatch(addUser(user));
};

const login = (email, password) => async (dispatch) => {
    const tokens = await postAuth('authenticate', { email, password });

    setTokens(tokens);

    // const { data: user } = await getApi('users', decodeToken(tokens.accessToken).sub);
    const { data: user } = await getApi('users', decodeToken(tokens.accessToken).sub);

    dispatch(addUser(user));
};

const logout = () => (dispatch) => {
    clearTokens();

    dispatch(removeUser());
};

export { setUser, login, logout };