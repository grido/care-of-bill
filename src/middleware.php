<?php

use App\Middleware\RenderMiddleware;
use App\Middleware\PaginationResolverMiddleware;

$app->add(new RenderMiddleware());

$app->add(new PaginationResolverMiddleware());
