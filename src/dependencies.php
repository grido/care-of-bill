<?php

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager();

$capsule->addConnection($container['settings']['database']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule) {
    return $capsule;
};

$container['logger'] = function (\Psr\Container\ContainerInterface $container) {
    $settings = $container->get('settings')['logger'];
    $logger = new \Monolog\Logger($settings['name']);

    $handlerSettings = $settings['handlers'][\Monolog\Handler\StreamHandler::class]();
    $handler = new \Monolog\Handler\StreamHandler($handlerSettings['path'], $handlerSettings['level']);
    $handler->setFormatter(new \Monolog\Formatter\JsonFormatter());

    $logger->pushHandler($handler);

    return $logger;
};

$container['errorHandler'] = function (\Psr\Container\ContainerInterface $container) {
    return new \App\Handlers\ExceptionHandler(
        $container['settings']['displayErrorDetails'],
        $container->get('logger')
    );
};

$container['auth'] = function (\Psr\Container\ContainerInterface $container) {
    $auth = new \App\Auth\Providers\Auth\EloquentProvider();
    $jwt = new \App\Auth\Providers\Jwt\FirebaseJwtProvider();

    return new \App\Auth\Auth($container->get('request'), $auth, $jwt);
};

$container['view'] = function (\Psr\Container\ContainerInterface $container) {
    $view = new \Slim\Views\PhpRenderer(__DIR__ . '/../templates/');
    $view->addAttribute('host', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);
    $view->addAttribute('router', $container->get('router'));
    $view->setLayout('layouts/email.phtml');

    return $view;
};

$container['mailer'] = function (\Psr\Container\ContainerInterface $container) {
    $config = $container->settings['mail'];

    $transport = (new Swift_SmtpTransport($config['host'], $config['port']))
        ->setUsername($config['username'])
        ->setPassword($config['password']);

    $swiftMailer = new \Swift_Mailer($transport);

    return (new \App\Mailer\Mailer($swiftMailer, $container->get('view'), $config['address']))
        ->setName($config['name']);
};

$container['UserValidation'] = function () {
    return new \App\Validation\UserValidation();
};

$container['UpdatePasswordValidation'] = function () {
    return new \App\Validation\UpdatePasswordValidation();
};

$container['ResetPasswordValidation'] = function () {
    return new \App\Validation\ResetPasswordValidation();
};

$container['UserAvatarUtil'] = function () {
    return new \App\Util\UserAvatarUtil();
};
