<?php declare(strict_types=1);

namespace App\Handlers;

use App\Exceptions\AuthException;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use Monolog\Logger;
use Slim\Handlers\Error as SlimHandler;

/**
 * Class ExceptionHandler
 * @package App\Handlers
 */
class ExceptionHandler extends SlimHandler
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ExceptionHandler constructor.
     * @param bool $displayErrorDetails
     * @param Logger $logger
     */
    public function __construct(bool $displayErrorDetails, Logger $logger)
    {
        $this->logger = $logger;

        parent::__construct($displayErrorDetails);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param \Exception $exception
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        \Exception $exception
    ): ResponseInterface {
        $data = [];
        $code = 500;

        switch (true) {
            case $exception instanceof AuthException:
                $this->handleAuthException($data, $exception);
                $code = 401;
                break;
            case $exception instanceof NotFoundException:
                $this->handleNotFoundException($data, $exception);
                $code = 404;
                break;
            case $exception instanceof ValidationException:
                $this->handleValidationException($data, $exception);
                $code = 422;
                break;
            case $exception instanceof ModelNotFoundException:
                $data = ['hint' => 'Not Found'];
                $code = 404;
                break;
            default:
                $this->logger->critical(
                    $exception->getMessage(),
                    ['exception' => $exception, 'trace' => $exception->getTraceAsString()]
                );
        }

//        return parent::__invoke($request, $response, $exception);
        return $response->withJson($data, $code);
    }

    /**
     * @param array $data
     * @param AuthException $exception
     */
    protected function handleAuthException(array &$data, AuthException $exception): void
    {
        if ($exception->hasHint()) {
            $data['hint'] = $exception->getHint();
        }
    }

    /**
     * @param array $data
     * @param NotFoundException $exception
     */
    protected function handleNotFoundException(array &$data, NotFoundException $exception): void
    {
        if ($exception->hasHint()) {
            $data['hint'] = $exception->getHint();
        }
    }

    /**
     * @param array $data
     * @param ValidationException $exception
     */
    protected function handleValidationException(array &$data, ValidationException $exception): void
    {
        if ($exception->hasErrors()) {
            $data['errors'] = $exception->getErrors();
        }
    }
}