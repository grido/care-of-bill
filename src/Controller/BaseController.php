<?php declare(strict_types=1);

namespace App\Controller;

use Psr\Container\ContainerInterface;

/**
 * Class BaseController
 * @package App\Controller
 */
abstract class BaseController
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * BaseController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function __get(string $property): object
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function get(string $property): object
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }
}