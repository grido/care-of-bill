<?php declare(strict_types=1);

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use App\Model\Bill;

/**
 * Class BillController
 * @package App\Controller
 */
class BillController extends BaseController
{

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function readAction(Request $request, Response $response): Response
    {
        $bills = Bill::get();

        return $response->withJson($bills, 200);
    }

}