<?php declare(strict_types=1);

namespace App\Controller;

use App\Exceptions\NotFoundException;
use App\Model\UserDescription;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Model\User;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends BaseController
{

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function readAction(Request $request, Response $response, array $args): Response
    {
        $user = User::findOrFail($args['id']);

        $this->get('UserAvatarUtil')->setUserAvatar($user);

        return $response->withJson(['data' => $user], 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function listAction(Request $request, Response $response): Response
    {
        $users = User::paginate();

        $users = [
            'data' => $users->toArray()['data'],
            'meta' => [
                'pagination' => array_except($users->toArray(), ['data'])
            ]
        ];

        return $response->withJson($users, 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function createAction(Request $request, Response $response): Response
    {
        $this->get('UserValidation')->validate($request);

        $data = $request->getParsedBody();

        $user = new User();
        $userDescription = new UserDescription();

        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->save();
        $userDescription->name = $data['user_description']['name'];
        $user->userDescription()->save($userDescription);

        return $response->withJson(['data' => $user->load('userDescription')], 201);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \App\Exceptions\BaseException|NotFoundException|ModelNotFoundException
     */
    public function updateProfileAction(Request $request, Response $response, array $args): Response
    {
        if ($request->getAttribute('user')['id'] !== (int)$args['id']) {
            throw (new NotFoundException())
                ->withHint('Resource not found');
        }

        $user = User::with('userDescription')->findOrFail($args['id']);

        $this->get('UserValidation')->validate($request, true);

        if (!empty($data = $request->getParsedBody())) {
            isset($data['email']) ? $user->email = $data['email'] : null;
            isset($data['user_description']['name']) ?
                $user->userDescription->name = $data['user_description']['name'] :
                null;

            $user->push();
        }

        return $response->withJson(['data' => $user], 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \App\Exceptions\BaseException|NotFoundException|ModelNotFoundException
     */
    public function updatePasswordAction(Request $request, Response $response, array $args): Response
    {
        if ($request->getAttribute('user')['id'] !== (int)$args['id']) {
            throw (new NotFoundException())
                ->withHint('Resource not found');
        }

        $user = User::findOrFail($args['id']);

        $this->get('UpdatePasswordValidation')->withAttributes($user->toArray(true))->validate($request);

        if (!empty($data = $request->getParsedBody())) {
            isset($data['password']) ? $user->password = $data['password'] : null;

            $user->save();
        }

        return $response->withJson(['data' => $user], 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \App\Exceptions\BaseException|NotFoundException|ModelNotFoundException
     */
    public function resetPasswordAction(Request $request, Response $response): Response
    {
        $rememberToken = $this->get('auth')->attemptRemember($request);

        $user = $rememberToken->user;

        $this->get('ResetPasswordValidation')->validate($request, true);

        if (!empty($data = $request->getParsedBody())) {
            isset($data['password']) ? $user->password = $data['password'] : null;

            $user->save();
        }

        return $response->withJson(['data' => $user], 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \App\Exceptions\BaseException|NotFoundException|ModelNotFoundException
     */
    public function deleteAction(Request $request, Response $response, array $args): Response
    {
        if ($request->getAttribute('user')['id'] !== (int)$args['id']) {
            throw (new NotFoundException())
                ->withHint('Resource not found');
        }

        $user = User::findOrFail($args['id']);

        $user->delete();

        return $response->withJson([], 204);
    }

}