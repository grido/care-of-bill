<?php declare(strict_types=1);

namespace App\Controller;

use App\Mailer\Mail\RememberMail;
use App\Model\User;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AuthController
 * @package App\Controller
 */
class AuthController extends BaseController
{

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function authAction(Request $request, Response $response): Response
    {
        $auth = $this->get('auth')->authenticate($request);

        return $response->withJson($auth, 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function refreshAction(Request $request, Response $response): Response
    {
        $auth = $this->get('auth')->refresh($request);

        return $response->withJson($auth, 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function rememberAction(Request $request, Response $response): Response
    {
        $auth = $this->get('auth')->userRemember($request);

        $this->get('mailer')->send(new RememberMail($request->getParam('email'), $auth));

        return $response->withJson([], 200);
    }
}
