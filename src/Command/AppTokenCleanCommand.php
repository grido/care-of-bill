<?php declare(strict_types=1);

namespace App\Command;

use App\Model\AuthAccessToken;
use Illuminate\Database\Eloquent\Collection;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppTokenCleanCommand extends Command
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AppTokenCleanCommand constructor.
     * @param ContainerInterface $container
     * @param string|null $name
     */
    public function __construct(ContainerInterface $container, ?string $name = null)
    {
        $this->container = $container;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setName('app:token:clean')
            ->addOption('seed', null, InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->cleanUpRevokedAndExpiredAuthTokens();

        $output->writeLn("Successful!");
    }

    protected function cleanUpRevokedAndExpiredAuthTokens(): void
    {
        $accessTokens = $this->getRevokedAndExpiredAccessTokens();

        foreach ($accessTokens as $accessToken) {
            $accessToken->delete();
        }
    }

    /**
     * @return Collection
     * @throws \Exception
     */
    protected function getRevokedAndExpiredAccessTokens(): Collection
    {
        return AuthAccessToken::with('refreshToken')
            ->whereDate('expires_at', '<=', new \DateTime())
            ->orWhere('revoked', '=', true)
            ->get();
    }
}
