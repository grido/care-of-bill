<?php declare(strict_types=1);

namespace App\Mailer;

use Slim\Views\PhpRenderer;
use Swift_Mailer;
use Swift_Message;

/**
 * Class Mailer
 * @package App\Mailer
 */
class Mailer
{
    /**
     * @var Swift_Mailer
     */
    protected $swiftMailer;

    /**
     * @var PhpRenderer
     */
    protected $view;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $name;

    /**
     * Mailer constructor.
     * @param Swift_Mailer $swiftMailer
     * @param PhpRenderer $view
     * @param string $address
     */
    public function __construct(Swift_Mailer $swiftMailer, PhpRenderer $view, ?string $address = null)
    {
        $this->swiftMailer = $swiftMailer;
        $this->view = $view;
        $this->address = $address;
    }

    /**
     * @param string $name
     * @return Mailer
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param MailInterface|array $mail
     * @param callable|null $callback
     * @return int
     */
    public function send($mail, ?callable $callback = null): int
    {
        $messageBuilder = $this->createMessageBuilder();

        if ($mail instanceof MailInterface) {
            $messageBuilder->body($this->parseView($mail->getView(), $mail->getViewData()));

            $mail->build($messageBuilder);
        } else {
            $messageBuilder->body($this->parseView($mail['view'], $mail['viewData']));

            call_user_func($callback, $messageBuilder);
        }

        return $this->swiftMailer->send($messageBuilder->getSwiftMessage());
    }

    /**
     * @return MessageBuilder
     */
    protected function createMessageBuilder(): MessageBuilder
    {
        return (new MessageBuilder(new Swift_Message()))->from($this->address, $this->name);
    }

    /**
     * @param string $view
     * @param array $viewData
     * @return string
     */
    protected function parseView(string $view, array $viewData): string
    {
        return $this->view->fetch($view, $viewData);
    }
}
