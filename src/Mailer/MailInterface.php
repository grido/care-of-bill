<?php declare(strict_types=1);

namespace App\Mailer;

/**
 * Interface MessageInterface
 * @package App\Mailer
 */
interface MailInterface
{

    /**
     * @param MessageBuilder $messageBuilder
     */
    public function build(MessageBuilder $messageBuilder): void;

    /**
     * @return string
     */
    public function getView(): string;

    /**
     * @return array
     */
    public function getViewData(): array;
}
