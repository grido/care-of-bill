<?php declare(strict_types=1);

namespace App\Mailer;

use Swift_Attachment;
use Swift_Message;

/**
 * Class MessageBuilder
 * @package App\Mailer
 */
class MessageBuilder
{
    /**
     * @var Swift_Message
     */
    protected $swiftMessage;

    /**
     * MessageBuilder constructor.
     * @param Swift_Message $swiftMessage
     */
    public function __construct(Swift_Message $swiftMessage)
    {
        $this->swiftMessage = $swiftMessage;
    }

    /**
     * @param string $address
     * @param string|null $name
     * @return MessageBuilder
     */
    public function to(string $address, ?string $name = null): self
    {
        $this->swiftMessage->setTo($address, $name);

        return $this;
    }

    /**
     * @param string $subject
     * @return MessageBuilder
     */
    public function subject(string $subject): self
    {
        $this->swiftMessage->setSubject($subject);

        return $this;
    }

    /**
     * @param string $file
     * @return MessageBuilder
     */
    public function attach(string $file): self
    {
        $this->swiftMessage->attach(Swift_Attachment::fromPath($file));

        return $this;
    }

    /**
     * @param string $body
     * @return MessageBuilder
     */
    public function body(string $body): self
    {
        $this->swiftMessage->setBody($body, 'text/html');

        return $this;
    }

    /**
     * @param string $address
     * @param string|null $name
     * @return MessageBuilder
     */
    public function from(string $address, ?string $name = null): self
    {
        $this->swiftMessage->addFrom($address, $name);

        return $this;
    }

    /**
     * @return Swift_Message
     */
    public function getSwiftMessage(): Swift_Message
    {
        return $this->swiftMessage;
    }
}
