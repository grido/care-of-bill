<?php declare(strict_types=1);

namespace App\Mailer\Mail;

use App\Mailer\MailInterface;
use App\Mailer\MessageBuilder;

/**
 * Class RememberMail
 * @package App\Mailer\Mail
 */
class RememberMail implements MailInterface
{

    /**
     * @var string
     */
    protected $to;

    /**
     * @var array
     */
    protected $data;

    /**
     * RememberMail constructor.
     * @param string $to
     * @param array $data
     */
    public function __construct(string $to, array $data)
    {
        $this->to = $to;
        $this->data = $data;
    }

    /**
     * @param MessageBuilder $messageBuilder
     */
    public function build(MessageBuilder $messageBuilder): void
    {
        $messageBuilder
            ->to($this->to)
            ->subject('Link aktywacyjny');
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return 'emails/remember.phtml';
    }

    /**
     * @return array
     */
    public function getViewData(): array
    {
        return $this->data;
    }
}
