<?php

use App\Controller\AuthController;
use App\Controller\BillController;
use App\Controller\UserController;
use App\Middleware\AuthMiddleware;

$app->group('/auth', function () use ($app) {
    $app->post('/authenticate', AuthController::class . ':authAction')
        ->setName('authAction');

    $app->get('/refresh', AuthController::class . ':refreshAction')
        ->setName('refreshAction');

    $app->post('/remember', AuthController::class . ':rememberAction')
        ->setName('rememberAction');
});

$app->group('/api', function () use ($app) {
    $app->post('/users', UserController::class . ':createAction')
        ->setName('usersCreateAction');

    $app->patch('/users/{id}/reset', UserController::class . ':resetPasswordAction')
        ->setName('usersResetPasswordAction');
});

$app->group('/api', function () use ($app) {
    $app->get('/bills', BillController::class . ':readAction')
        ->setName('billsReadAction');

    $app->get('/users/{id}', UserController::class . ':readAction')
        ->setName('usersReadAction');

    $app->get('/users', UserController::class . ':listAction')
        ->setName('usersListAction');

    $app->patch('/users/{id}/profile', UserController::class . ':updateProfileAction')
        ->setName('usersUpdateProfileAction');

    $app->patch('/users/{id}/password', UserController::class . ':updatePasswordAction')
        ->setName('usersUpdatePasswordAction');

    $app->delete('/users/{id}', UserController::class . ':deleteAction')
        ->setName('usersDeleteAction');
})->add(new AuthMiddleware($container));


$app->get('/api/test', function ($request, $response) use ($container) {
    $response->getBody()->write("It works!");
    return $response;
});
