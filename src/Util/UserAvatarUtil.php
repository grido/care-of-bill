<?php declare(strict_types=1);

namespace App\Util;

use App\Model\User;
use GuzzleHttp\Client;

/**
 * Class UserAvatarUtil
 * @package App\Util
 */
class UserAvatarUtil
{

    /**
     * @param User $user
     */
    public function setUserAvatar(User $user): void
    {
        $hash = md5($user->email);

        if ($this->doesAvatarExist($hash)) {
            $user->userDescription->avatar = $hash;
        } else {
            $user->userDescription->avatar = null;
        }

        $user->push();
    }

    /**
     * @param string $hash
     * @return bool
     */
    private function doesAvatarExist(string $hash): bool
    {
        $client = new Client(['http_errors' => false]);

        $res = $client->get('https://www.gravatar.com/avatar/' . $hash . '?d=404');

        return $res->getStatusCode() === 200;
    }
}
