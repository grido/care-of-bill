<?php declare(strict_types=1);

namespace App\Validation;

/**
 * Class UpdatePasswordValidation
 * @package App\Validation
 */
class UpdatePasswordValidation extends BaseValidation
{

    /**
     * @return array
     */
    protected function rules(): array
    {
        $currentPassword = $this->attributes['password'];

        return [
            'current_password' => [
                'required',
                ['currentPassword', $currentPassword]
            ],
            'password' => ['required', ['lengthMin', 8], ['lengthMax', 20]],
            'confirm_password' => [
                'required',
                ['lengthMin', 8],
                ['lengthMax', 20],
                ['equals', 'password', 'confirm_password']
            ],
        ];
    }
}
