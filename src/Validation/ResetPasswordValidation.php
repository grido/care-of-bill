<?php declare(strict_types=1);

namespace App\Validation;

/**
 * Class ResetPasswordValidation
 * @package App\Validation
 */
class ResetPasswordValidation extends BaseValidation
{

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            'password' => ['required', ['lengthMin', 8], ['lengthMax', 20]],
            'confirm_password' => [
                'required',
                ['lengthMin', 8],
                ['lengthMax', 20],
                ['equals', 'password', 'confirm_password']
            ],
        ];
    }
}
