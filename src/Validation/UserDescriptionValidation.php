<?php declare(strict_types=1);

namespace App\Validation;

/**
 * Class UserDescriptionValidation
 * @package App\Validation
 */
class UserDescriptionValidation extends BaseValidation
{

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            'name' => ['required', ['unique', 'UserDescription'], ['lengthMax', 250]]
        ];
    }
}
