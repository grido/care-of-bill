<?php declare(strict_types=1);

namespace App\Validation;

/**
 * Class UserValidation
 * @package App\Validation
 */
class UserValidation extends BaseValidation
{

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            'email' => ['required', ['unique', 'User'], 'email', ['lengthMax', 250]],
            'password' => ['required', ['lengthMin', 8], ['lengthMax', 20]],
            'confirm_password' => [
                'required',
                ['lengthMin', 8],
                ['lengthMax', 20],
                ['equals', 'password', 'confirm_password']
            ],
        ];
    }

    /**
     * @return array
     */
    protected function relatedValidations(): array
    {
        return [
            'user_description' => UserDescriptionValidation::class
        ];
    }
}
