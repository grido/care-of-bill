<?php declare(strict_types=1);

namespace App\Validation;

use App\Exceptions\ValidationException;
use Slim\Http\Request;
use Valitron\Validator;

/**
 * Class BaseValidation
 * @package App\Validation
 */
abstract class BaseValidation
{

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var array
     */
    protected $rules;

    /**
     * @var array
     */
    protected $relatedValidations;

    public function __construct()
    {
        $this->setUpCustomRules();
    }

    /**
     * @param array $attributes
     * @return BaseValidation
     */
    public function withAttributes(array $attributes): self
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @param Request $request
     * @param bool $optional
     * @throws ValidationException
     */
    public function validate(Request $request, bool $optional = false): void
    {
        $data = $request->getParsedBody();

        if ($errors = $this->getErrors($data, $optional)) {
            throw (new ValidationException())
                ->withErrors($errors);
        }
    }

    /**
     * @param array $data
     * @param bool $optional
     * @return array
     */
    public function getErrors(array $data, bool $optional): array
    {
        $this->setRules();

        if ($optional) {
            $this->transformRules($data);
        }

        $validator = new Validator($data);

        $validator->mapFieldsRules($this->rules);

        $errors = !$validator->validate() ? $errors = $validator->errors() : [];

        return array_merge($errors, $this->getRelatedValidationErrors($data, $optional));
    }

    /**
     * @param array $data
     * @param bool $optional
     * @return array
     */
    private function getRelatedValidationErrors(array $data, bool $optional): array
    {
        $errors = [];

        $this->setRelatedValidations();

        foreach ($this->relatedValidations as $name => $validation) {
            $validationData = is_array($data[$name]) ? $data[$name] : [];

            $relatedValidationErrors = (new $validation())->getErrors($validationData, $optional);

            if ($relatedValidationErrors) {
                $errors[$name] = $relatedValidationErrors;
            }
        }

        return $errors;
    }

    private function setUpCustomRules(): void
    {
        $rulesNameSpace = "\\App\\Validation\\Rules\\";

        $rules = new \DirectoryIterator(__DIR__ . "/Rules");

        foreach ($rules as $ruleInfo) {
            if ($ruleInfo->isFile()) {
                $ruleName = $ruleInfo->getBasename(".php");

                $rule = $rulesNameSpace . $ruleName;
                $rule = new $rule();

                Validator::addRule(lcfirst($ruleName), $rule->rule(), $rule->message());
            }
        }
    }

    private function setRules(): void
    {
        $this->rules = $this->rules();
    }

    private function setRelatedValidations(): void
    {
        $this->relatedValidations = $this->relatedValidations();
    }

    /**
     * @param array $data
     */
    private function transformRules(array $data): void
    {
        $this->rules = array_filter($this->rules, function ($field) use ($data) {
            return isset($data[$field]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function relatedValidations(): array
    {
        return [];
    }
}