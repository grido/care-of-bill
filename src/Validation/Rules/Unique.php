<?php declare(strict_types=1);

namespace App\Validation\Rules;

/**
 * Class Unique
 * @package App\Validation\Rules
 */
class Unique
{
    public function rule(): callable
    {
        return function ($field, $value, array $params) {
            $model = '\\App\\Model\\' . $params[0];

            return (new $model())->where($field, $value)->doesntExist();
        };
    }

    public function message(): string
    {
        return 'is already exist';
    }
}
