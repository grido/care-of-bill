<?php declare(strict_types=1);

namespace App\Validation\Rules;

use App\Model\User;

/**
 * Class CurrentPassword
 * @package App\Validation\Rules
 */
class CurrentPassword
{
    public function rule(): callable
    {
        return function ($field, $value, array $params) {
            $currentPassword = $params[0];

            return password_verify($value, $currentPassword);
        };
    }

    public function message(): string
    {
        return 'is not correct';
    }
}
