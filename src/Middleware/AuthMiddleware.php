<?php declare(strict_types=1);

namespace App\Middleware;

use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AuthMiddleware
 * @package App\Middleware
 */
class AuthMiddleware
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AuthMiddleware constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        $user = $this->container->get('auth')->authorization($request);

        $request = $request->withAttribute('user', $user);

        return $next($request, $response);
    }
}
