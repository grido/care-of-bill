<?php declare(strict_types=1);

namespace App\Middleware;

use App\Exceptions\NotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class RenderMiddleware
 * @package App\Middleware
 */
class RenderMiddleware
{

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     * @throws \App\Exceptions\BaseException|NotFoundException
     */
    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        $route = $request->getUri()->getPath();

        if (strpos($route, 'auth') === false && strpos($route, 'api') === false) {
            $file = __DIR__ . '/../../public/index.html';

            if (file_exists($file)) {
                return $response->withBody(new Stream(fopen($file, 'r')));
            } else {
                //todo: override slim not found exception
                throw (new NotFoundException())
                    ->withHint('Page not found');
            }
        }

        return $next($request, $response);
    }
}
