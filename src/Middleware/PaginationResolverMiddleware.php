<?php declare(strict_types=1);

namespace App\Middleware;

use Illuminate\Pagination\Paginator;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PaginationResolverMiddleware
 * @package App\Middleware
 */
class PaginationResolverMiddleware
{

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        Paginator::currentPathResolver(function () use ($request) {
            return $request->getUri()->getPath();
        });

        Paginator::currentPageResolver(function () use ($request) {
            return $request->getParam('page', 1);
        });

        $response = $next($request, $response);

        return $response;
    }

}