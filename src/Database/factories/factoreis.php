<?php

$factory(\App\Model\User::class, [
    'email' => $faker->email,
    'password' => 'test',
]);

$factory(\App\Model\UserDescription::class, [
    'name' => $faker->name,
    'experience' => $faker->numberBetween(0, 1000000),
]);

$factory(\App\Model\Bill::class, [
    'cost' => $faker->randomFloat(2, '0.1', 100)
]);
