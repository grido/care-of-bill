<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuthAccessToken extends Migration
{

    public function up()
    {
        $this->schema->create('auth_access_token', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->unsignedInteger('user_id');
            $table->integer('revoked')->default(0);
            $table->dateTime('expires_at');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    public function down()
    {
        $this->schema->drop('auth_access_token');
    }
}
