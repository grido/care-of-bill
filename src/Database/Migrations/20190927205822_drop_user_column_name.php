<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class DropUserColumnName extends Migration
{

    public function up()
    {
        $this->schema->table('user', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }

    public function down()
    {
        $this->schema->table('user', function (Blueprint $table) {
            $table->string('name');
        });
    }
}
