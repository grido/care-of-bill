<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDescription extends Migration
{

    public function up()
    {
        $this->schema->create('user_description', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name')->unique();
            $table->integer('experience')->default(0);
            $table->string('avatar')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    public function down()
    {
        $this->schema->drop('user_description');
    }
}
