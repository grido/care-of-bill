<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuthRefreshToken extends Migration
{

    public function up()
    {
        $this->schema->create('auth_refresh_token', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('access_token_id', 36);
            $table->integer('revoked')->default(0);
            $table->dateTime('expires_at');
            $table->timestamps();

            $table->foreign('access_token_id')->references('id')->on('auth_access_token');
        });
    }

    public function down()
    {
        $this->schema->drop('auth_refresh_token');
    }
}
