<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class ForeignBillToUser extends Migration
{

    public function up()
    {
        $this->schema->table('bill', function (Blueprint $table) {
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    public function down()
    {
        $this->schema->create('bill', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
