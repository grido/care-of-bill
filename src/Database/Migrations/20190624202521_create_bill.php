<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBill extends Migration
{

    public function up()
    {
        $this->schema->create('bill', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cost');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('bill');
    }
}
