<?php

use App\Database\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUser extends Migration
{

    public function up()
    {
        $this->schema->create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('user');
    }
}
