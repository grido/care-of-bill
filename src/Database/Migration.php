<?php

namespace App\Database;

use Illuminate\Database\Capsule\Manager;
use Phinx\Migration\AbstractMigration;

class Migration extends AbstractMigration
{

    protected $schema;

    public function init(): void
    {
        $this->schema = (new Manager())->schema();
    }

}
