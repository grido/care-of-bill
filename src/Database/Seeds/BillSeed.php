<?php

use App\Model\Bill;
use App\Model\User;
use Illuminate\Database\Capsule\Manager;
use Laracasts\TestDummy\Factory;
use Phinx\Seed\AbstractSeed;

class BillSeed extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'UserSeed',
        ];
    }

    public function run()
    {
        Manager::statement('SET FOREIGN_KEY_CHECKS=0;');

        Bill::query()->truncate();

        $userCount = User::count();

        for ($i = 1; $i <= $userCount; $i++) {
            Factory::times(15)->create(Bill::class, [
                'user_id' => $i
            ]);
        }

        Manager::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
