<?php

use App\Model\AuthRefreshToken;
use App\Model\AuthAccessToken;
use App\Model\AuthRememberToken;
use App\Model\User;
use Illuminate\Database\Capsule\Manager;
use Laracasts\TestDummy\Factory;
use Phinx\Seed\AbstractSeed;

class UserSeed extends AbstractSeed
{

    public function run()
    {
        Manager::statement('SET FOREIGN_KEY_CHECKS=0;');

        AuthRefreshToken::truncate();
        AuthAccessToken::truncate();
        AuthRememberToken::truncate();
        User::truncate();

        for ($i = 1; $i <= 3; $i++) {
            Factory::create(User::class, [
                'email' => 'u' . $i . '@c.com'
            ]);
        }

        Manager::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
