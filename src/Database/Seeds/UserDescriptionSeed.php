<?php


use App\Model\User;
use App\Model\UserDescription;
use Illuminate\Database\Capsule\Manager;
use Laracasts\TestDummy\Factory;
use Phinx\Seed\AbstractSeed;

class UserDescriptionSeed extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'UserSeed',
        ];
    }

    public function run()
    {
        Manager::statement('SET FOREIGN_KEY_CHECKS=0;');

        UserDescription::truncate();

        $userCount = User::count();

        for ($i = 1; $i <= $userCount; $i++) {
            Factory::create(UserDescription::class, [
                'user_id' => $i
            ]);
        }

        Manager::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
