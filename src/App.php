<?php

namespace App;

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

class App
{

    /**
     * @var \Slim\App
     */
    protected $app;

    /**
     * App constructor.
     */
    public function __construct()
    {
        try {
            (Dotenv::create(__DIR__ . '/../'))->load();
        } catch (InvalidPathException $e) {
            header('Content-Type: application/json');
            header('X-Error-Message: Internal server error', true, 500);
            exit(json_encode(['error' => 'Internal error']));
        }

        $settings = require __DIR__ . '/settings.php';
        $app = new \Slim\App($settings);

        // Set Dependencies
        require __DIR__ . '/dependencies.php';

        // Set Routes
        require __DIR__ . '/routes.php';

        // Set Middleware
        require __DIR__ . '/middleware.php';

        $this->app = $app;
    }

    /**
     * @return \Slim\App
     */
    public function get(): \Slim\App
    {
        return $this->app;
    }
}
