<?php declare(strict_types=1);

namespace App\Model;

use App\Support\Model\AuthTokenTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class AuthAccessToken
 * @package App\Model
 */
class AuthAccessToken extends Model
{
    use AuthTokenTrait;

    /**
     * @var string
     */
    protected $table = 'auth_access_token';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'revoked',
        'expires_at'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function refreshToken(): HasOne
    {
        return $this->hasOne(AuthRefreshToken::class, 'access_token_id', 'id');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function delete(): bool
    {
        $this->refreshToken()->delete();
        return parent::delete();
    }
}
