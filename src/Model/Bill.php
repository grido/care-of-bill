<?php declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Bill
 * @package App\Model
 */
class Bill extends Model
{
    /**
     * @var string
     */
    protected $table = 'bill';

    /**
     * @var array
     */
    protected $fillable = [
        'cost'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}