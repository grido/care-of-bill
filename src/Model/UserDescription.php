<?php declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UserDescription
 * @package App\Model
 */
class UserDescription extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_description';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'experience',
        'avatar'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}