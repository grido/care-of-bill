<?php declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class User
 * @package App\Model
 */
class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type'
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'type' => 1
    ];

    /**
     * @var array
     */
    protected $excludes = [
        'password'
    ];

    /**
     * @return HasMany
     */
    public function accessTokens(): HasMany
    {
        return $this->hasMany(AuthAccessToken::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function rememberTokens(): HasMany
    {
        return $this->hasMany(AuthRememberToken::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function bills(): HasMany
    {
        return $this->hasMany(Bill::class, 'user_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function userDescription(): HasOne
    {
        return $this->hasOne(UserDescription::class, 'user_id', 'id');
    }

    /**
     * @param string $password
     */
    public function setPasswordAttribute(string $password): void
    {
        $this->attributes['password'] = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function delete(): bool
    {
        foreach ($this->accessTokens()->get() as $object) {
            $object->delete();
        }

        foreach ($this->rememberTokens()->get() as $object) {
            $object->delete();
        }

        foreach ($this->bills()->get() as $object) {
            $object->delete();
        }

        $this->userDescription()->delete();

        return parent::delete();
    }

    /**
     * @param bool $withExcludes
     * @return array
     */
    public function toArray($withExcludes = false): array
    {
        if ($withExcludes) {
            return parent::toArray();
        }

        return array_filter(parent::toArray(), function ($key) {
            return !in_array($key, $this->excludes);
        }, ARRAY_FILTER_USE_KEY);
    }
}