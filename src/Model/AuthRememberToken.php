<?php declare(strict_types=1);

namespace App\Model;

use App\Support\Model\AuthTokenTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AuthRememberToken extends Model
{
    use AuthTokenTrait;

    /**
     * @var string
     */
    protected $table = 'auth_remember_token';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'token',
        'revoked',
        'expires_at'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param string $token
     */
    public function setTokenAttribute(string $token): void
    {
        $this->attributes['token'] = hash('sha256', $token);
    }
}
