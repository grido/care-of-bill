<?php declare(strict_types = 1);

namespace App\Model;

use App\Support\Model\AuthTokenTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class AuthRefreshToken
 * @package App\Model
 */
class AuthRefreshToken extends Model
{
    use AuthTokenTrait;

    /**
     * @var string
     */
    protected $table = 'auth_refresh_token';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'access_token_id',
        'revoked',
        'expires_at'
    ];

    /**
     * @return BelongsTo
     */
    public function accessToken(): BelongsTo
    {
        return $this->belongsTo('App\Model\AuthAccessToken', 'access_token_id', 'id');
    }

}