<?php declare(strict_types=1);

namespace App\Support\Model;

trait AuthTokenTrait
{
    /**
     * @param int $expires
     * @throws \Exception
     */
    public function setExpiresAtAttribute(int $expires): void
    {
        $this->attributes['expires_at'] = (new \DateTime())->setTimestamp($expires)->format('Y-m-d H:i:s');
    }

    public function revoke(): void
    {
        $this->update(['revoked' => true]);
    }
}
