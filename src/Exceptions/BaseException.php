<?php declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class BaseException
 * @package App\Exceptions
 */
class BaseException extends \Exception
{
    /**
     * @var string
     */
    protected $hint = null;

    /**
     * @param string $hint
     * @return self
     */
    public function withHint(string $hint): self
    {
        $this->hint = $hint;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasHint(): bool
    {
        return is_string($this->hint);
    }

    /**
     * @return string
     */
    public function getHint(): string
    {
        return $this->hint;
    }
}
