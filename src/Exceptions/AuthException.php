<?php declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class AuthException
 * @package App\Exceptions
 */
class AuthException extends BaseException
{
}
