<?php declare(strict_types=1);

namespace App\Exceptions;

class ValidationException extends BaseException
{

    /**
     * @var array
     */
    private $error = [];

    /**
     * @param array $error
     * @return ValidationException
     */
    public function withErrors(array $error): ValidationException
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !empty($this->error);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->error;
    }
}
