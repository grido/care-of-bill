<?php declare(strict_types=1);

namespace App\Auth;

use Ramsey\Uuid\Uuid;

/**
 * Class RememberFactory
 * @package App\Auth
 */
class RememberPropsFactory
{

    /**
     * @var array
     */
    protected $props = [
        'id',
        'token',
        'expires'
    ];

    /**
     * @var string
     */
    protected $expiry;

    /**
     * RememberPropsFactory constructor.
     * @param string $expiry
     */
    public function __construct(string $expiry)
    {
        $this->expiry = (int)$expiry;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function id(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function token(): string
    {
        return random_bytes(32);
    }

    /**
     * @return int
     */
    public function expires(): int
    {
        return time() + $this->expiry;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return array_map(
            function ($claim) {
                if (method_exists($this, $claim)) {
                    return $this->$claim();
                }
            },
            array_combine($this->props, $this->props)
        );
    }
}
