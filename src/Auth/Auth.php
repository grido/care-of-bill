<?php declare(strict_types=1);

namespace App\Auth;

use App\Auth\Providers\Auth\AuthProviderInterface;
use App\Auth\Providers\Jwt\JwtProviderInterface;
use App\Model\AuthRememberToken;
use App\Model\User;
use Slim\Http\Request;

/**
 * Class Auth
 * @package App\Auth
 */
class Auth
{
    /** @var AuthProviderInterface */
    protected $auth;
    /** @var JwtProviderInterface */
    protected $jwt;
    /** @var ClaimsFactory */
    protected $accessClaimFactory;
    /** @var ClaimsFactory */
    protected $refreshClaimFactory;
    /** @var RememberPropsFactory */
    protected $rememberPropsFactory;

    /**
     * Auth constructor.
     * @param Request $request
     * @param AuthProviderInterface $auth
     * @param JwtProviderInterface $jwt
     */
    public function __construct(Request $request, AuthProviderInterface $auth, JwtProviderInterface $jwt)
    {
        $this->auth = $auth;
        $this->jwt = $jwt;
        $this->accessClaimFactory = new ClaimsFactory($request, getenv('ACCESS_EXPIRY'));
        $this->refreshClaimFactory = new ClaimsFactory($request, getenv('REFRESH_EXPIRY'));
        $this->rememberPropsFactory = new RememberPropsFactory(getenv('REMEMBER_EXPIRY'));
    }

    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AuthException
     * @throws \App\Exceptions\BaseException
     */
    public function authenticate(Request $request): array
    {
        $user = $this->auth->userByCredentials($request);

        return $this->initAuthTokens($user);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function refresh(Request $request): array
    {
        $decodedRefreshToken = $this->jwt->decodeRefreshToken($request);

        $accessToken = $this->auth->accessTokenById($decodedRefreshToken->sub);

        $accessToken->refreshToken->revoke();
        $accessToken->revoke();

        return $this->initAuthTokens($accessToken->user);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AuthException
     * @throws \App\Exceptions\BaseException
     */
    public function authorization(Request $request): array
    {
        $decodedAccessToken = $this->jwt->decodeAccessToken($request);

        $accessToken = $this->auth->accessTokenById($decodedAccessToken->jti);

        return $accessToken->user->toArray();
    }

    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AuthException
     * @throws \App\Exceptions\BaseException
     */
    public function userRemember(Request $request): array
    {
        $user = $this->auth->userByEmail($request);

        return $this->initRememberToken($user);
    }

    /**
     * @param Request $request
     * @return AuthRememberToken
     * @throws \App\Exceptions\AuthException
     * @throws \App\Exceptions\BaseException
     */
    public function attemptRemember(Request $request): AuthRememberToken
    {
        $id = $request->getAttribute('id');

        $rememberToken = $this->auth->rememberTokenById($id);

        $this->auth->attemptRememberToken($request, $rememberToken);

        return $rememberToken;
    }

    /**
     * @param User $user
     * @return array
     */
    protected function initAuthTokens(User $user): array
    {
        $accessPayload = $this->makePayload((string)$user->id, $this->accessClaimFactory);
        $refreshPayload = $this->makePayload($accessPayload['jti'], $this->refreshClaimFactory);

        if ($tokens = $this->jwt->encodeTokens($accessPayload, $refreshPayload)) {
            $this->auth->setAuthTokens($user, $accessPayload, $refreshPayload);
        }

        return $tokens;
    }

    /**
     * @param string $subject
     * @param ClaimsFactory $claimsFactory
     * @return array
     */
    protected function makePayload(string $subject, ClaimsFactory $claimsFactory): array
    {
        return $claimsFactory->get(['sub' => $subject]);
    }

    /**
     * @param User $user
     * @return array
     * @throws \Exception
     */
    protected function initRememberToken(User $user): array
    {
        $rememberProps = $this->rememberPropsFactory->get();

        $this->auth->setRememberToken($user, $rememberProps);

        return [
            'id' => $rememberProps['id'],
            'token' => bin2hex($rememberProps['token']),
        ];
    }

}