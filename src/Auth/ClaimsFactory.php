<?php declare(strict_types=1);

namespace App\Auth;

use Ramsey\Uuid\Uuid;
use Slim\Http\Request;

/**
 * Class ClaimsFactory
 * @package App\Auth
 */
class ClaimsFactory
{

    /**
     * @var array
     */
    protected $defaultClaims = [
        'iss',
        'iat',
        'exp',
        'nbf',
        'jti'
    ];

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $expiry;

    /**
     * ClaimsFactory constructor.
     * @param Request $request
     * @param string $expiry
     */
    public function __construct(Request $request, string $expiry)
    {
        $this->request = $request;
        $this->expiry = (int)$expiry;
    }

    /**
     * issuer - Identifies principal that issued the JWT
     * @return string
     */
    public function iss(): string
    {
        return $this->request->getUri()->getPath();
    }

    /**
     * Issued at - Identifies the time at which the JWT was issued
     * @return int
     */
    public function iat(): int
    {
        return time();
    }

    /**
     * Not Before - Identifies the time on which the JWT will start to be accepted for processing
     * @return int
     */
    public function nbf(): int
    {
        return time();
    }

    /**
     * JWT ID - Case sensitive unique identifier of the token even among different issuers
     * @return string
     * @throws \Exception
     */
    public function jti(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * Expiration Time - Identifies the expiration time on and after which the JWT must not be accepted for processing
     * @return int
     */
    public function exp(): int
    {
        return time() + $this->expiry;
    }

    /**
     * @param array $additionalClaims
     * @return array
     */
    public function get(array $additionalClaims = []): array
    {
        $claims = array_map(
            function ($claim) {
                if (method_exists($this, $claim)) {
                    return $this->$claim();
                }
            },
            array_combine($this->defaultClaims, $this->defaultClaims)
        );

        return array_merge($additionalClaims, $claims);
    }
}