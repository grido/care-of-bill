<?php declare(strict_types=1);

namespace App\Auth\Providers\Jwt;

use App\Auth\AccessToken;
use App\Auth\RefreshToken;
use App\Exceptions\AuthException;
use Firebase\JWT\JWT;
use Slim\Http\Request;

/**
 * Class FirebaseJwtProvider
 * @package App\Auth\Providers\Jwt
 */
class FirebaseJwtProvider implements JwtProviderInterface
{
    /**
     * @param Request $request
     * @return string
     * @throws \App\Exceptions\BaseException|AuthException
     */
    protected function getAuthorizationHeader(Request $request): string
    {
        if (!list($header) = $request->getHeader('Authorization', false)) {
            throw (new AuthException())->withHint('Required token');
        }

        return $header;
    }

    /**
     * @param array $payload
     * @param string $secret
     * @return string
     */
    protected function encode(array $payload, string $secret): string
    {
        return JWT::encode($payload, $secret);
    }

    /**
     * @param string $token
     * @param string $secret
     * @return \stdClass
     * @throws \App\Exceptions\BaseException|AuthException
     */
    protected function decode(string $token, string $secret): \stdClass
    {
        try {
            return JWT::decode($token, $secret, ['HS256']);
        } catch (\Exception $e) {
            throw (new AuthException('', 0, $e))->withHint('Invalid token');
        }
    }

    /**
     * @param array $accessPayload
     * @param array $refreshPayload
     * @return array
     */
    public function encodeTokens(array $accessPayload, array $refreshPayload): array
    {
        return [
            'accessToken' => $this->encode($accessPayload, getenv('ACCESS_SECRET')),
            'refreshToken' => $this->encode($refreshPayload, getenv('REFRESH_SECRET'))
        ];
    }

    /**
     * @param Request $request
     * @return AccessToken|\stdClass
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function decodeAccessToken(Request $request): \stdClass
    {
        $token = $this->getAuthorizationHeader($request);

        return $this->decode($token, env('ACCESS_SECRET'));
    }

    /**
     * @param Request $request
     * @return RefreshToken|\stdClass
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function decodeRefreshToken(Request $request): \stdClass
    {
        $token = $this->getAuthorizationHeader($request);

        return $this->decode($token, env('REFRESH_SECRET'));
    }
}
