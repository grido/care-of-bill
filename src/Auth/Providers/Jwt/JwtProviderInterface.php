<?php declare(strict_types = 1);

namespace App\Auth\Providers\Jwt;

use App\Auth\AccessToken;
use App\Auth\RefreshToken;
use Slim\Http\Request;

/**
 * Interface JwtProviderInterface
 * @package App\Auth\Providers\Jwt
 */
interface JwtProviderInterface
{
    /**
     * @param array $accessPayload
     * @param array $refreshPayload
     * @return array
     */
    public function encodeTokens(array $accessPayload, array $refreshPayload): array;

    /**
     * @param Request $request
     * @return AccessToken|\stdClass
     */
    public function decodeAccessToken(Request $request): \stdClass;

    /**
     * @param Request $request
     * @return RefreshToken|\stdClass
     */
    public function decodeRefreshToken(Request $request): \stdClass;
}
