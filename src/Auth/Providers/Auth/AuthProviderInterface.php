<?php declare(strict_types = 1);

namespace App\Auth\Providers\Auth;

use App\Exceptions\AuthException;
use App\Model\AuthAccessToken;
use App\Model\AuthRememberToken;
use App\Model\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Http\Request;

/**
 * Interface AuthProviderInterface
 * @package App\Auth\Providers\Auth
 */
interface AuthProviderInterface
{
    /**
     * @param Request $request
     * @return User
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userByCredentials(Request $request): User;

    /**
     * @param Request $request
     * @return User
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userByEmail(Request $request): User;

    /**
     * @param Request $request
     * @param AuthRememberToken $rememberToken
     */
    public function attemptRememberToken(Request $request, AuthRememberToken $rememberToken): void;

    /**
     * @param string $id
     * @return AuthAccessToken
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function accessTokenById(string $id): AuthAccessToken;

    /**
     * @param string $id
     * @return AuthRememberToken
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function rememberTokenById(string $id): AuthRememberToken;

    /**
     * @param User $user
     * @param array $accessPayload
     * @param array $refreshPayload
     */
    public function setAuthTokens(User $user, array $accessPayload, array $refreshPayload): void;

    /**
     * @param User $user
     * @param array $rememberProps
     * @throws \Exception
     */
    public function setRememberToken(User $user, array $rememberProps): void;
}
