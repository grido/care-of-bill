<?php declare(strict_types=1);

namespace App\Auth\Providers\Auth;

use App\Auth\UserCredential;
use App\Exceptions\AuthException;
use App\Model\AuthAccessToken;
use App\Model\AuthRefreshToken;
use App\Model\AuthRememberToken;
use App\Model\User;
use Slim\Http\Request;

/**
 * Class EloquentProvider
 * @package App\Auth\Providers\Auth
 */
class EloquentProvider implements AuthProviderInterface
{
    /**
     * @param Request $request
     * @return User
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userByCredentials(Request $request): User
    {
        $userCredentials = new UserCredential($request);

        if (!$user = User::with('accessTokens')->where('email', $userCredentials->email)->first()) {
            throw (new AuthException())
                ->withHint('User does not exists');
        }

        if (!password_verify($userCredentials->password, $user->password)) {
            throw (new AuthException())
                ->withHint('Incorrect password');
        }

        return $user;
    }

    /**
     * @param Request $request
     * @return User
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userByEmail(Request $request): User
    {
        $userCredentials = new UserCredential($request, ['email']);

        if (!$user = User::query()->where('email', $userCredentials->email)->first()) {
            throw (new AuthException())
                ->withHint('User does not exists');
        }

        return $user;
    }

    public function attemptRememberToken(Request $request, AuthRememberToken $rememberToken): void
    {
        $requestBody = $request->getParsedBody();

        if (!$token = $requestBody['token']) {
            throw (new AuthException())
                ->withHint('Token required');
        }

        if (!ctype_xdigit($token) || !hash_equals(hash('sha256', hex2bin($token)), $rememberToken->token)) {
            throw (new AuthException())
                ->withHint('Invalid token');
        }
    }

    /**
     * @param string $id
     * @return AuthAccessToken
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function accessTokenById(string $id): AuthAccessToken
    {
        if (!$accessToken = AuthAccessToken::with('user', 'refreshToken')->find($id)) {
            throw (new AuthException())
                ->withHint('Access token not found');
        }

        if ($accessToken->revoked || $accessToken->refreshToken->revoked) {
            throw (new AuthException())
                ->withHint('Revoked token');
        }

        return $accessToken;
    }

    /**
     * @param string $id
     * @return AuthRememberToken
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function rememberTokenById(string $id): AuthRememberToken
    {
        if (!$rememberToken = AuthRememberToken::with('user')->find($id)) {
            throw (new AuthException())
                ->withHint('Remember token not found');
        }

        if ($rememberToken->revoked || strtotime($rememberToken->expires_at) <= time()) {
            throw (new AuthException())
                ->withHint('Revoked token');
        }

        return $rememberToken;
    }

    /**
     * @param User $user
     * @param array $accessPayload
     * @param array $refreshPayload
     * @throws \Exception
     */
    public function setAuthTokens(User $user, array $accessPayload, array $refreshPayload): void
    {
        (new AuthAccessToken([
            'id' => $accessPayload['jti'],
            'expires_at' => $accessPayload['exp'],
            'user_id' => $user->id
        ]))->save();

        (new AuthRefreshToken([
            'id' => $refreshPayload['jti'],
            'expires_at' => $refreshPayload['exp'],
            'access_token_id' => $accessPayload['jti']
        ]))->save();
    }

    /**
     * @param User $user
     * @param array $rememberProps
     * @throws \Exception
     */
    public function setRememberToken(User $user, array $rememberProps): void
    {
        (new AuthRememberToken([
            'id' => $rememberProps['id'],
            'token' => $rememberProps['token'],
            'expires_at' => $rememberProps['expires'],
            'user_id' => $user->id,
        ]))->save();
    }
}