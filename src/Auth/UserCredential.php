<?php declare(strict_types=1);

namespace App\Auth;

use App\Exceptions\AuthException;
use Slim\Http\Request;

/**
 * Class UserCredential
 * @package App\Auth
 */
class UserCredential
{

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * UserCredential constructor.
     * @param Request $request
     * @param array $only
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function __construct(Request $request, array $only = [])
    {
        if (!$requestBody = $request->getParsedBody()) {
            throw (new AuthException())
                ->withHint('User credential required');
        }

        $this->mapCredential($requestBody, $only);
    }

    /**
     * @param array $requestBody
     * @param array $only
     * @throws AuthException
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function mapCredential(array $requestBody, array $only): void
    {

        if (!$only || in_array('email', $only)) {
            $this->email = $this->userEmail($requestBody);
        }

        if (!$only || in_array('password', $only)) {
            $this->password = $this->userPassword($requestBody);
        }
    }

    /**
     * @param array $requestBody
     * @return string
     * @throws \App\Exceptions\BaseException|AuthException
     */
    protected function userEmail(array $requestBody): string
    {
        if (!$email = $requestBody['email']) {
            throw (new AuthException())
                ->withHint('Email required');
        }

        return $email;
    }

    /**
     * @param array $requestBody
     * @return string
     * @throws \App\Exceptions\BaseException|AuthException
     */
    protected function userPassword(array $requestBody): string
    {
        if (!$password = $requestBody['password']) {
            throw (new AuthException())
                ->withHint('Password required');
        }

        return $password;
    }
}