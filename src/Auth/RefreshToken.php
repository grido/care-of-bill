<?php
declare(strict_types=1);

namespace App\Auth;

/**
 * Class RefreshToken
 * @package App\Auth
 *
 * @property string $jti Json Token Id
 * @property int $exp timestamp of expiry
 * @property string $iss Authorization url path
 * @property int $iat timestamp now()
 * @property int $nbf timestamp now()
 * @property mixed $sub Id of access token
 */
class RefreshToken
{

}
