<?php

return [
    'settings' => [
        'displayErrorDetails' => env('APP_DEBUG', false),
        'dbQueryLogging' => false,
        'database' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_NAME'),
            'username' => env('DB_USER'),
            'password' => env('DB_PASSWORD'),
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => ''
        ],
        'mail' => [
            'host' => getenv('MAIL_HOST'),
            'port' => getenv('MAIL_PORT'),
            'username' => getenv('MAIL_USERNAME'),
            'password' => getenv('MAIL_PASSWORD'),
            'address' => getenv('MAIL_FROM_ADDRESS'),
            'name' => getenv('MAIL_FROM_NAME'),
        ],
        'logger' => [
            'name' => 'app-log-' . date('Y-m-d'),
            'handlers' => [
                \Monolog\Handler\StreamHandler::class => function () {
                    return [
                        'path' => __DIR__ . '/../logs/' . date('Y-m-d') . '.log',
                        'level' => \Monolog\Logger::DEBUG,
                    ];
                },
            ],
        ],
    ],
];
