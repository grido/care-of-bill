<?php declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * Class BaseUnitTest
 * @package Tests
 */
abstract class BaseUnitTest extends TestCase
{

    /**
     * @param string $uri
     * @param array|null $body
     * @param array $params
     * @return Request
     */
    protected function makePostJsonRequest(string $uri, ?array $body = null, array $params = []): Request
    {
        $env = Environment::mock(array_merge([
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => 'POST',
            'CONTENT_TYPE' => 'application/json'
        ], $params));

        return Request::createFromEnvironment($env)->withParsedBody($body);
    }
}
