<?php declare(strict_types=1);

namespace Tests;

use App\App;
use App\Model\AuthAccessToken;
use App\Model\AuthRefreshToken;
use App\Model\AuthRememberToken;
use App\Model\User;
use Illuminate\Database\Capsule\Manager;
use Laracasts\TestDummy\Factory;
use Phinx\Console\PhinxApplication;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

/**
 * Class BaseFunctionalTest
 * @package Tests
 */
abstract class BaseFunctionalTest extends TestCase
{

    /**
     * @var App
     */
    protected $app;

    /**
     * @var \stdClass
     */
    protected $auth;

    /**
     * @var bool
     */
    protected $authRequired = true;

    public function setUp(): void
    {
        Factory::$factoriesPath = __DIR__ . '/../src/Database/factories';

        $this->runDatabaseMigration();

        $this->appSetUp();

        if ($this->authRequired) {
            $this->authSetUp();
        }
    }

    public function tearDown(): void
    {
        Manager::statement('SET FOREIGN_KEY_CHECKS=0;');
        AuthRefreshToken::truncate();
        AuthAccessToken::truncate();
        AuthRememberToken::truncate();
        User::truncate();
        Manager::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->app = null;

        $this->auth = null;
    }

    /**
     * @throws \Exception
     */
    protected function runDatabaseMigration(): void
    {
        $phinx = new PhinxApplication();
        $phinx->setAutoExit(false);
        $phinx->run(new StringInput('migrate'), new NullOutput());
    }

    protected function appSetUp(): void
    {
        $this->app = (new App())->get();
    }

    protected function authSetUp(): void
    {
        $user = Factory::create(User::class, ['email' => 'test@test.org']);

        $this->auth = $this->authenticateUser(['email' => $user->email, 'password' => 'test']);

        $this->auth->id = $user->id;
    }

    /**
     * @param array $credential
     * @return \stdClass
     */
    protected function authenticateUser(array $credential): \stdClass
    {
        $response = $this->makePostJsonResponse('/auth/authenticate', $credential);

        return json_decode((string)$response->getBody());
    }

    /**
     * @param string $uri
     * @param array $params
     * @return Response
     */
    protected function makeGetJsonResponse(string $uri, array $params = []): Response
    {
        $env = Environment::mock(array_merge([
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => 'GET',
            'CONTENT_TYPE' => 'application/json'
        ], $params));

        $request = Request::createFromEnvironment($env);

        $this->app->getContainer()['request'] = $request;

        return $this->app->run(true);
    }

    /**
     * @param string $uri
     * @param array|null $body
     * @param array $params
     * @return Response
     */
    protected function makePostJsonResponse(string $uri, ?array $body = null, array $params = []): Response
    {
        $env = Environment::mock(array_merge([
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => 'POST',
            'CONTENT_TYPE' => 'application/json'
        ], $params));

        $request = Request::createFromEnvironment($env)->withParsedBody($body);

        $this->app->getContainer()['request'] = $request;

        return $this->app->run(true);
    }

    /**
     * @param string $uri
     * @param array|null $body
     * @param array $params
     * @return Response
     */
    protected function makePatchJsonResponse(string $uri, ?array $body = null, array $params = []): Response
    {
        $env = Environment::mock(array_merge([
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => 'PATCH',
            'CONTENT_TYPE' => 'application/json'
        ], $params));

        $request = Request::createFromEnvironment($env)->withParsedBody($body);

        $this->app->getContainer()['request'] = $request;

        return $this->app->run(true);
    }

    protected function makeDeleteJsonResponse(string $uri, array $params = []): Response
    {
        $env = Environment::mock(array_merge([
            'REQUEST_URI' => $uri,
            'REQUEST_METHOD' => 'DELETE',
            'CONTENT_TYPE' => 'application/json'
        ], $params));

        $request = Request::createFromEnvironment($env);

        $this->app->getContainer()['request'] = $request;

        return $this->app->run(true);
    }
}
