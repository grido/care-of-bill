<?php declare(strict_types=1);

namespace Tests\Functional\AuthController;

use App\Model\User;
use Laracasts\TestDummy\Factory;
use Tests\BaseFunctionalTest;

/**
 * Class AuthControllerTest
 * @package Test\Controller
 */
abstract class AuthControllerTest extends BaseFunctionalTest
{

    /**
     * @var bool
     */
    protected $authRequired = false;

    /**
     * @var User|array
     */
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->userSetUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();

        $this->user = null;
    }

    protected function userSetUp(): void
    {
        $this->user = Factory::create(User::class, [
            'email' => 'test@test.org'
        ]);
    }

    /**
     * @return array
     */
    public function incorrectCredentialProvider(): array
    {
        return [
            [
                []
            ],
            [
                [
                    'email' => '',
                    'password' => '',
                ]
            ],
            [
                [
                    'email' => 'test@test.org',
                    'password' => '',
                ]
            ],
            [
                [
                    'email' => '',
                    'password' => 'test',
                ]
            ],
            [
                [
                    'email' => 'fakeUser@test.org',
                    'password' => 'test',
                ]
            ],
            [
                [
                    'email' => 'test@test.org',
                    'password' => 'fakePassword',
                ]
            ]
        ];
    }

    public function incorrectEmailProvider()
    {
        return [
            [
                ''
            ],
            [
                'fakeEmail@test.org'
            ]
        ];
    }

    /**
     * @return array
     */
    public function incorrectAuthorizationTokenProvider(): array
    {
        return [
            [
                ''
            ],
            [
                'fakeAuthorizationToken'
            ]
        ];
    }
}
