<?php declare(strict_types=1);

namespace Tests\Functional\AuthController;

/**
 * Class GetTest
 * @package Tests\Functional\AuthController
 */
class GetTest extends AuthControllerTest
{

    /**
     * @test
     */
    public function refreshActionReturnsAuthenticationData(): void
    {
        $auth = json_decode((string)($this->makePostJsonResponse('/auth/authenticate', [
            'email' => 'test@test.org',
            'password' => 'test'
        ]))->getBody());

        $response = $this->makeGetJsonResponse('/auth/refresh', [
            'HTTP_AUTHORIZATION' => $auth->refreshToken
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('accessToken', $data);
        $this->assertObjectHasAttribute('refreshToken', $data);
    }

    /**
     * @test
     * @dataProvider incorrectAuthorizationTokenProvider
     * @param string $token
     */
    public function thatRefreshActionWithIncorrectAuthorizationHeaderReturnsUnauthorizedStatus(string $token): void
    {
        $response = $this->makeGetJsonResponse('/auth/refresh', [
            'HTTP_AUTHORIZATION' => $token
        ]);

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', json_decode((string)$response->getBody()));
    }
}
