<?php declare(strict_types=1);

namespace Tests\Functional\AuthController;

/**
 * Class PostTest
 * @package Tests\Functional\AuthController
 */
class PostTest extends AuthControllerTest
{

    /**
     * @test
     * @return \stdClass
     */
    public function authActionReturnsAuthenticationData(): \stdClass
    {
        $response = $this->makePostJsonResponse('/auth/authenticate', [
            'email' => 'test@test.org',
            'password' => 'test'
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('accessToken', $data);
        $this->assertObjectHasAttribute('refreshToken', $data);

        return $data;
    }

    /**
     * @test
     */
    public function rememberActionReturnsOkStatus()
    {
        $response = $this->makePostJsonResponse('/auth/remember', [
            'email' => 'test@test.org'
        ]);

        $this->assertSame(200, $response->getStatusCode());
    }

    /**
     * @test
     * @dataProvider incorrectCredentialProvider
     * @param array $credentials
     */
    public function authActionWithIncorrectCredentialsReturnsUnauthorizedStatus(array $credentials): void
    {
        $response = $this->makePostJsonResponse('/auth/authenticate', $credentials);

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', json_decode((string)$response->getBody()));
    }

    /**
     * @test
     * @dataProvider incorrectEmailProvider
     * @param string $email
     */
    public function rememberActionWithIncorrectEmailReturnsUnauthorizedStatus(string $email)
    {
        $response = $this->makePostJsonResponse('/auth/remember', [
            'email' => $email
        ]);

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', json_decode((string)$response->getBody()));
    }
}