<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

use App\Auth\RememberPropsFactory;
use App\Model\AuthRememberToken;
use App\Model\User;
use App\Model\UserDescription;
use Illuminate\Database\Capsule\Manager;
use Laracasts\TestDummy\Factory;
use Tests\BaseFunctionalTest;

/**
 * Class UserControllerTest
 * @package Tests\Functional\UserController
 */
abstract class UserControllerTest extends BaseFunctionalTest
{

    /**
     * @var User|array
     */
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        if ($this->auth) {
            $this->userSetUp();
        }
    }

    public function tearDown(): void
    {
        parent::tearDown();

        Manager::statement('SET FOREIGN_KEY_CHECKS=0;');
        UserDescription::truncate();
        Manager::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->user = null;
    }

    protected function userSetUp(): void
    {
        $this->user = User::find($this->auth->id);

        Factory::create(UserDescription::class, ['name' => 'test', 'user_id' => $this->auth->id]);
    }

    /**
     * @return string
     */
    protected function createUserRememberToken(): string
    {
        $rememberProps = (new RememberPropsFactory('3600'))->get();

        (new AuthRememberToken([
            'id' => $rememberProps['id'],
            'token' => $rememberProps['token'],
            'expires_at' => $rememberProps['expires'],
            'user_id' => $this->user->id,
        ]))->save();

        return bin2hex($rememberProps['token']);
    }

    /**
     * @return array
     */
    public function correctUserProvider(): array
    {
        return [
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'fakeEmail@test.org',
                    'password' => 'fakePassword',
                    'confirm_password' => 'fakePassword',
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function correctPasswordProvider(): array
    {
        return [
            [
                [
                    'current_password' => 'test',
                    'password' => 'fakePassword',
                    'confirm_password' => 'fakePassword',
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function incorrectUserProvider(): array
    {
        return [
            [
                [
                    'user_description' => [
                        'name' => 'test'
                    ],
                    'email' => 'test@test.org',
                    'password' => 'fakePassword',
                    'confirm_password' => 'fakePassword',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'incorrectEmail',
                    'password' => 'fakePassword',
                    'confirm_password' => 'fakePassword',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'fakeEmail@test.org',
                    'password' => 'fakePassword',
                    'confirm_password' => '',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'fakeEmail@test.org',
                    'password' => 'fakePassword',
                    'confirm_password' => 'notSamePassword',
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function incorrectUserProfileProvider(): array
    {
        return [
            [
                [
                    'user_description' => [
                        'name' => 'test'
                    ],
                    'email' => 'fakeEmail@test.org',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'test@test.org',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => ''
                    ],
                    'email' => 'fakeEmail@test.org',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => '',
                ]
            ],
            [
                [
                    'user_description' => [
                        'name' => 'fakeName'
                    ],
                    'email' => 'incorrectEmail',
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    public function incorrectPasswordProvider(): array
    {
        return [
            [
                [
                    'current_password' => '',
                    'password' => '',
                    'confirm_password' => '',
                ]
            ],
            [
                [
                    'current_password' => 'incorrectPassword',
                    'password' => '',
                    'confirm_password' => '',
                ]
            ],
            [
                [
                    'current_password' => 'test',
                    'password' => '',
                    'confirm_password' => '',
                ]
            ],
            [
                [
                    'current_password' => 'test',
                    'password' => 'fakePassword',
                    'confirm_password' => '',
                ]
            ],
            [
                [
                    'current_password' => 'test',
                    'password' => 'fakePassword',
                    'confirm_password' => 'notSamePassword',
                ]
            ],
        ];
    }
}