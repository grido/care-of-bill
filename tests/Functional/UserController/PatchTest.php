<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

use App\Model\AuthRememberToken;
use App\Model\User;
use Laracasts\TestDummy\Factory;

/**
 * Class PatchTest
 * @package Tests\Functional\UserController
 */
class PatchTest extends UserControllerTest
{

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function updateProfileActionReturnsUpdatedUser(array $correctUser): void
    {
        $response = $this->makePatchJsonResponse('/api/users/' . $this->auth->id . '/profile', $correctUser, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody(), true);

        $this->user->refresh()->load('userDescription');

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($this->user->toArray(), $data['data']);
    }

    /**
     * @test
     * @dataProvider correctPasswordProvider
     * @param array $correctPassword
     */
    public function updatePasswordActionReturnsUpdatedUser(array $correctPassword): void
    {
        $response = $this->makePatchJsonResponse(
            '/api/users/' . $this->auth->id . '/password',
            $correctPassword,
            ['HTTP_AUTHORIZATION' => $this->auth->accessToken]
        );

        $data = json_decode((string)$response->getBody(), true);

        $this->user->refresh();

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($this->user->toArray(), $data['data']);
    }

    /**
     * @test
     * @dataProvider correctPasswordProvider
     * @param array $correctPassword
     */
    public function resetPasswordActionReturnsUpdatedUser(array $correctPassword): void
    {
        $token = $this->createUserRememberToken();

        $rememberToken = AuthRememberToken::query()->where('user_id', $this->user->id)->first();

        $response = $this->makePatchJsonResponse(
            '/api/users/' . $rememberToken->id . '/reset',
            array_merge(['token' => $token], $correctPassword)
        );

        $this->user->refresh();

        $data = json_decode((string)$response->getBody(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($this->user->toArray(), $data['data']);
    }

    /**
     * @test
     * @dataProvider incorrectUserProfileProvider
     * @param array $correctUserProfile
     */
    public function updateProfileActionValidateData(array $correctUserProfile): void
    {
        $response = $this->makePatchJsonResponse(
            '/api/users/' . $this->auth->id . '/profile',
            $correctUserProfile,
            ['HTTP_AUTHORIZATION' => $this->auth->accessToken]
        );

        $data = json_decode((string)$response->getBody());

        $this->assertSame(422, $response->getStatusCode());
        $this->assertObjectHasAttribute('errors', $data);
    }

    /**
     * @test
     * @dataProvider incorrectPasswordProvider
     * @param array $incorrectPassword
     */
    public function updatePasswordActionValidateData(array $incorrectPassword): void
    {
        $response = $this->makePatchJsonResponse(
            '/api/users/' . $this->auth->id . '/password',
            $incorrectPassword,
            ['HTTP_AUTHORIZATION' => $this->auth->accessToken]
        );

        $data = json_decode((string)$response->getBody());

        $this->assertSame(422, $response->getStatusCode());
        $this->assertObjectHasAttribute('errors', $data);
    }

    /**
     * @test
     * @dataProvider incorrectPasswordProvider
     * @param array $incorrectPassword
     */
    public function resetPasswordActionValidateData(array $incorrectPassword): void
    {
        $token = $this->createUserRememberToken();

        $rememberToken = AuthRememberToken::query()->where('user_id', $this->user->id)->first();

        $response = $this->makePatchJsonResponse(
            '/api/users/' . $rememberToken->id . '/reset',
            array_merge(['token' => $token], $incorrectPassword)
        );

        $data = json_decode((string)$response->getBody());

        $this->assertSame(422, $response->getStatusCode());
        $this->assertObjectHasAttribute('errors', $data);
    }

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function updateProfileActionWithoutAuthorizationHeaderReturnsUnauthorizedStatus(array $correctUser): void
    {
        $response = $this->makePatchJsonResponse('/api/users/' . $this->auth->id . '/profile', $correctUser);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function updatePasswordActionWithoutAuthorizationHeaderReturnsUnauthorizedStatus(array $correctUser): void
    {
        $response = $this->makePatchJsonResponse('/api/users/' . $this->auth->id . '/password', $correctUser);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     * @dataProvider correctPasswordProvider
     * @param array $correctPassword
     */
    public function resetPasswordActionWithIncorrectRememberTokenReturnsUnauthorizedStatus(
        array $correctPassword
    ): void {
        $this->createUserRememberToken();

        $rememberToken = AuthRememberToken::query()->where('user_id', $this->user->id)->first();

        $response = $this->makePatchJsonResponse(
            '/api/users/' . $rememberToken->id . '/reset',
            array_merge(['token' => 'fakeRememberToken'], $correctPassword)
        );

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function whenUpdateProfileActionTryingToUpdateAnotherUserReturnsNotFoundStatus(array $correctUser): void
    {
        $newUser = Factory::create(User::class);

        $response = $this->makePatchJsonResponse('/api/users/' . $newUser->id . '/profile', $correctUser, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(404, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function whenUpdatePasswordActionTryingToUpdateAnotherUserReturnsNotFoundStatus(array $correctUser): void
    {
        $newUser = Factory::create(User::class);

        $response = $this->makePatchJsonResponse('/api/users/' . $newUser->id . '/password', $correctUser, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(404, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     * @dataProvider correctPasswordProvider
     * @param array $correctPassword
     */
    public function resetPasswordActionWithIncorrectRememberTokenIdReturnsUnauthorizedStatus(
        array $correctPassword
    ): void {
        $token = $this->createUserRememberToken();

        $response = $this->makePatchJsonResponse(
            '/api/users/' . 1 . '/reset',
            array_merge(['token' => $token], $correctPassword)
        );

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }
}
