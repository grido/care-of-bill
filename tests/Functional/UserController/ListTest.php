<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

use App\Model\User;
use Laracasts\TestDummy\Factory;

/**
 * Class ListTest
 * @package Tests\Functional\UserController
 */
class ListTest extends UserControllerTest
{
    protected function userSetUp(): void
    {
        $this->user = Factory::times(rand(10, 20))->create(User::class);
    }

    /**
     * @test
     */
    public function listActionsReturnsListOffUsers(): void
    {
        $response = $this->makeGetJsonResponse('/api/users', [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('data', $data);
        $this->assertObjectHasAttribute('meta', $data);
        $this->assertContainsOnly(\stdClass::class, $data->data);
    }

    /**
     * @test
     */
    public function listActionWithoutAuthorizationHeaderReturnsUnauthorizedStatus(): void
    {
        $response = $this->makeGetJsonResponse('/api/users');

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
    }
}
