<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

/**
 * Class GetTest
 * @package Tests\Functional\UserController
 */
class GetTest extends UserControllerTest
{

    /**
     * @test
     */
    public function readActionReturnsUserById(): void
    {
        $response = $this->makeGetJsonResponse('/api/users/' . $this->auth->id, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody(), true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($this->user->load('userDescription')->toArray(), $data['data']);
    }

    /**
     * @test
     */
    public function readActionWithoutAuthorizationHeaderReturnsUnauthorizedStatus(): void
    {
        $response = $this->makeGetJsonResponse('/api/users/' . $this->auth->id);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
    }
}
