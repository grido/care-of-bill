<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

use App\Model\User;

/**
 * Class PostTest
 * @package Tests\Functional\UserController
 */
class PostTest extends UserControllerTest
{

    /**
     * @test
     * @dataProvider correctUserProvider
     * @param array $correctUser
     */
    public function createActionReturnsNewUser(array $correctUser): void
    {
        $response = $this->makePostJsonResponse('/api/users', $correctUser);

        $data = json_decode((string)$response->getBody(), true);

        $user = User::with('userDescription')->find($data['data']['id']);

        $this->assertSame(201, $response->getStatusCode());
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($user->toArray(), $data['data']);
    }

    /**
     * @test
     * @dataProvider incorrectUserProvider
     * @param array $incorrectUser
     */
    public function createActionValidateData(array $incorrectUser): void
    {
        $response = $this->makePostJsonResponse('/api/users', $incorrectUser);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(422, $response->getStatusCode());
        $this->assertObjectHasAttribute('errors', $data);
    }
}