<?php declare(strict_types=1);

namespace Tests\Functional\UserController;

use App\Model\User;
use Laracasts\TestDummy\Factory;

/**
 * Class DeleteTest
 * @package Tests\Functional\UserController
 */
class DeleteTest extends UserControllerTest
{

    /**
     * @test
     */
    public function deleteActionReturnsNoContent(): void
    {
        $response = $this->makeDeleteJsonResponse('/api/users/' . $this->auth->id, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $this->assertSame(204, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteActionWithoutAuthorizationHeaderReturnsUnauthorizedStatus(): void
    {
        $response = $this->makeDeleteJsonResponse('/api/users/' . $this->auth->id);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(401, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }

    /**
     * @test
     */
    public function whenDeleteActionTryingToDeleteAnotherUserReturnsForbiddenStatus(): void
    {
        $newUser = Factory::create(User::class);

        $response = $this->makeDeleteJsonResponse('/api/users/' . $newUser->id, [
            'HTTP_AUTHORIZATION' => $this->auth->accessToken
        ]);

        $data = json_decode((string)$response->getBody());

        $this->assertSame(404, $response->getStatusCode());
        $this->assertObjectHasAttribute('hint', $data);
        $this->assertStringContainsStringIgnoringCase('hint', (string)$response->getBody());
    }
}