<?php declare(strict_types=1);

namespace Tests\Unit\Auth;

use App\Auth\UserCredential;
use App\Exceptions\AuthException;
use Faker\Factory;
use Tests\BaseUnitTest;

/**
 * Class UserCredentialTest
 * @package Tests\Auth
 */
class UserCredentialTest extends BaseUnitTest
{

    /**
     * @test
     * @dataProvider correctUserCredentialProvider
     * @param array $credential
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userCredentialCanBeSelectedFromRequest(array $credential): void
    {
        $request = $this->makePostJsonRequest('', $credential);

        $userCredential = new UserCredential($request);

        $this->assertSame($credential['email'], $userCredential->email);
        $this->assertSame($credential['password'], $userCredential->password);
    }

    /**
     * @test
     * @dataProvider correctUserCredentialProvider
     * @param array $credential
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function singleCredentialCanBeSelectedFromRequest(array $credential): void
    {
        $request = $this->makePostJsonRequest('', $credential);

        $userCredential = new UserCredential($request, ['email']);

        $this->assertSame($credential['email'], $userCredential->email);
        $this->assertSame(null, $userCredential->password);
    }

    /**
     * @test
     * @dataProvider incorrectUserCredentialProvider
     * @param array|null $credential
     * @throws \App\Exceptions\BaseException|AuthException
     */
    public function userCredentialThrowAuthException(?array $credential): void
    {
        $this->expectException(AuthException::class);

        $request = $this->makePostJsonRequest('', $credential);

        new UserCredential($request);
    }

    /**
     * @return array
     */
    public function correctUserCredentialProvider(): array
    {
        $faker = Factory::create();

        return [
            [
                'credential' => ['email' => $faker->email, 'password' => $faker->password(10)]
            ]
        ];
    }

    /**
     * @return array
     */
    public function incorrectUserCredentialProvider(): array
    {
        $faker = Factory::create();

        return [
            [
                null,
            ],
            [
                [],
            ],
            [
                ['email' => $faker->email, 'password' => ''],
            ],
            [
                ['email' => '', 'password' => $faker->password(10)],
            ],
        ];
    }

}
