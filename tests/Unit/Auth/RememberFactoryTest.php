<?php declare(strict_types=1);

namespace Tests\Unit\Auth;

use App\Auth\RememberPropsFactory;
use Tests\BaseUnitTest;

/**
 * Class RememberFactoryTest
 * @package Tests\Unit\Auth
 */
class RememberFactoryTest extends BaseUnitTest
{

    /**
     * @var string
     */
    protected $expiry = '3600';

    /**
     * @var RememberPropsFactory
     */
    protected $rememberPropsFactory;

    public function setUp(): void
    {
        $this->rememberPropsFactory = new RememberPropsFactory($this->expiry);
    }

    /**
     * @test
     */
    public function idMethodReturnsUUID(): void
    {
        $jti = $this->rememberPropsFactory->id();

        $this->assertIsString($jti);
        $this->assertSame(36, strlen($jti));
        $this->assertRegExp('/^[a-z-A-Z0-9\-]+$/', $jti);
    }

    /**
     * @test
     */
    public function tokenMethodReturnsRandomToken(): void
    {
        $token = $this->rememberPropsFactory->token();

        $this->assertIsString($token);
        $this->assertSame(32, strlen($token));
    }

    /**
     * @test
     */
    public function expiresMethodReturnsCurrentPlusExpireTime(): void
    {
        $expires = $this->rememberPropsFactory->expires();

        $expireTime = time() + (int)$this->expiry;

        $this->assertIsInt($expires);
        $this->assertSame($expireTime, $expires);
    }

    /**
     * @test
     */
    public function getMethodReturnsArrayWithRememberData(): void
    {
        $props = $this->rememberPropsFactory->get();

        $requiredRememberProps = ['id', 'token', 'expires'];

        $this->assertTrue(is_array($props));

        foreach ($requiredRememberProps as $prop) {
            $this->assertArrayHasKey($prop, $props);
        }
    }
}
