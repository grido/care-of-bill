<?php declare(strict_types = 1);

namespace Tests\Unit\Auth;

use App\Auth\ClaimsFactory;
use Faker\Factory;
use Tests\BaseUnitTest;

/**
 * Class ClaimsFactoryTest
 */
class ClaimsFactoryTest extends BaseUnitTest
{

    /**
     * @var string
     */
    protected $URI;

    /**
     * @var string
     */
    protected $expiry = '3600';

    /**
     * @var ClaimsFactory
     */
    protected $claimsFactory;

    public function setUp(): void
    {
        $this->URI = '/auth/authenticate';

        $request = $this->makePostJsonRequest($this->URI);

        $this->claimsFactory = new ClaimsFactory($request, $this->expiry);
    }

    /**
     * @test
     */
    public function issuerMethodReturnsRequestPath(): void
    {
        $iss = $this->claimsFactory->iss();

        $this->assertIsString($iss);
        $this->assertSame($this->URI, $iss);
    }

    /**
     * @test
     */
    public function issuedAtMethodReturnsCurrentTime(): void
    {
        $iat = $this->claimsFactory->iat();

        $this->assertIsInt($iat);
        $this->assertSame(time(), $iat);
    }

    /**
     * @test
     */
    public function notBeforeMethodReturnsCurrentTime(): void
    {
        $nbf = $this->claimsFactory->nbf();

        $this->assertIsInt($nbf);
        $this->assertSame(time(), $nbf);
    }

    /**
     * @test
     */
    public function jsonWebTokenIdMethodReturnsUUID(): void
    {
        $jti = $this->claimsFactory->jti();

        $this->assertIsString($jti);
        $this->assertSame(36, strlen($jti));
        $this->assertRegExp('/^[a-z-A-Z0-9\-]+$/', $jti);
    }

    /**
     * @test
     */
    public function expirationTimeMethodReturnsCurrentPlusExpireTime(): void
    {
        $exp = $this->claimsFactory->exp();

        $expireTime = time() + (int)$this->expiry;

        $this->assertIsInt($exp);
        $this->assertSame($expireTime, $exp);
    }

    /**
     * @test
     */
    public function getClaimsMethodReturnsArrayWithDefaultClaims(): void
    {
        $claims = $this->claimsFactory->get();

        $defaultClaims = ['iss', 'iat', 'exp', 'nbf', 'jti'];

        $this->assertTrue(is_array($claims));

        foreach ($defaultClaims as $claim) {
            $this->assertArrayHasKey($claim, $claims);
        }
    }

    /**
     * @test
     * @dataProvider additionalClaimsProvider
     * @param array $additionalClaims
     */
    public function getClaimsMethodReturnsArrayWithAdditionalClaims(array $additionalClaims): void
    {
        $claims = $this->claimsFactory->get($additionalClaims);

        $this->assertTrue(is_array($claims));

        foreach ($additionalClaims as $claim => $value) {
            $this->assertArrayHasKey($claim, $claims);
            $this->assertSame($additionalClaims[$claim], $claims[$claim]);
        }
    }

    /**
     * @return array
     */
    public function additionalClaimsProvider(): array
    {
        $faker = Factory::create();

        return [
            [
                ['sub' => $faker->randomNumber]
            ],
            [
                ['sub' => $faker->randomNumber, 'aud' => $faker->randomNumber]
            ]
        ];
    }
}
