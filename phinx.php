<?php

require __DIR__ . '/vendor/autoload.php';

\Laracasts\TestDummy\Factory::$factoriesPath = __DIR__ . '/src/Database/factories';

$app = (new \App\App())->get();

$app->run();

$config = $app->getContainer()['settings']['database'];

return [
    'migration_base_class' => 'App\Database\Migration',
    'templates' => [
        'file' => 'src/Database/Templates/MigrationTemplate',
    ],
    'paths' => [
        'migrations' => 'src/Database/Migrations',
        'seeds' => 'src/Database/Seeds'
    ],
    'environments' =>
        [
            'default_migration_table' => 'migrations',
            'default_database' => 'development',
            'development' => [
                'adapter' => $config['driver'],
                'host' => $config['host'],
                'port' => $config['port'],
                'name' => $config['database'],
                'user' => $config['username'],
                'pass' => $config['password'],
                'charset' => $config['charset']
            ]
        ]
];