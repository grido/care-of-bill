<?php

require __DIR__ . '/vendor/autoload.php';

$application = new \Symfony\Component\Console\Application();

$commands = new \DirectoryIterator(__DIR__ . '/src/Command');

$commandsNameSpace = "\\App\\Command\\";

// Set up all commands in $commandsNameSpace
foreach ($commands as $commandInfo) {
    if ($commandInfo->isFile()) {
        $commandName = $commandInfo->getBasename('.php');

        $command = $commandsNameSpace . $commandName;

        $application->add(new $command((new \App\App())->get()->getContainer()));
    }
}

$application->run();
